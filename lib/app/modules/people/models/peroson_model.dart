// ignore_for_file: overridden_fields

import '../../person/domain/entities/person_entity.dart';
import '../../person/domain/value_objects/type_profiles.dart';

class PersonModel extends PersonEntity {
  @override
  final String id;
  @override
  final String name;
  @override
  final String cpf;
    @override
  final String email;

  @override
  final Profiles profiles;

  PersonModel({
    required this.id,
    required this.name,
    required this.cpf,
    required this.email,
    required this.profiles,
  }) : super(id: id, name: name, cpf: cpf,email: email, profiles: profiles,);

  PersonModel copyWith({
    String? id,
    String? name,
    String? cpf,
    String? email,
    Profiles? profiles,
  }) {
    return PersonModel(
      id: id ?? this.id,
        name:name ??   this.name,
     cpf:cpf??  this.cpf,
     email: email ?? this.email,
     profiles: profiles ?? this.profiles,
    );
  }

  factory PersonModel.empty() {
    return PersonModel(
   id :'',
    name:'',
    cpf:'',
    email: '',
    profiles:Profiles.user,
    );
  }
}
