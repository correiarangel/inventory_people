import 'package:flutter_modular/flutter_modular.dart';
import 'package:inventory_people/app/modules/people/presenter/pages/people_page.dart';
import 'blocs/person_bloc.dart';

class PeopleModule extends Module {
  @override
  final List<Bind> binds = [
    Bind(
      ((i) => PersonBloc(
            i.get(),
            i.get(),
            i.get(),
            i.get(),
            i.get(),
          )),
    ),
  ];
  @override
  final List<ModularRoute> routes = [
    ChildRoute(
      '/people',
      child: (context, args) => PeoplePage(
        personBloc: Modular.get<PersonBloc>(),
      ),
    ),
  ];
}
