import 'package:flutter/material.dart';
import '../../../person/domain/entities/person_entity.dart';


class CardPerson extends StatelessWidget {
  final PersonEntity person;
  final int index;

  final double width;
  final double height;
  CardPerson({
    required this.person,
    required this.index,
    required this.width,
    required this.height,
    Key? key,
  }) : super(key: key);
  final itemKey = DateTime.now().microsecondsSinceEpoch.toString();
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Dismissible(
        key: Key(itemKey),
        direction: DismissDirection.endToStart,
        onDismissed: (direction) {},
        background: Container(
          color: Colors.red[800],
          padding: const EdgeInsets.all(15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: const <Widget>[
              Icon(
                Icons.delete,
                color: Colors.grey,
              )
            ],
          ),
        ),
        //listaView de tarefas
        child: ListTile(
          leading: const Icon(Icons.person),
          title: Text(
            person.name,
            style: TextStyle(
              color: Colors.grey[800],
            ),
          ),
          subtitle: Text(
            person.cpf,
            style: TextStyle(
              color: Colors.grey[700],
            ),
          ),
        ),
      ),
    );
  }
}
