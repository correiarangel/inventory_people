import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../blocs/event/person_event.dart';
import '../../blocs/state/person_state.dart';
import '../../blocs/person_bloc.dart';

import '../components/card_person.dart';

class PeoplePage extends StatefulWidget {
  final PersonBloc personBloc;
  const PeoplePage({Key? key, required this.personBloc}) : super(key: key);

  @override
  State<PeoplePage> createState() => _PeoplePageState();
}

class _PeoplePageState extends State<PeoplePage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.personBloc.add(GetAllPersonEvent());
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height / 100;
    final width = MediaQuery.of(context).size.width / 100;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Inventario de  Pessoas '),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BlocBuilder(
            bloc: widget.personBloc,
            builder: (context, state) {
              if (state is LoadedPersonState) {
                return SizedBox(
                  height: height * 80,
                  child: ListView.builder(
                    itemCount: state.personList.length,
                    itemBuilder: (context, index) {
                      final person = state.personList[index];

                      return CardPerson(
                        person: person,
                        index: index,
                        width: width * 90,
                        height: height * 10,
                      );
                    },
                  ),
                );
              }
              if (state is LoadingPersonState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return const Center(
                child: Text('Não há pessoas cadastradas'),
              );
            },
          ),
        ],
      ),
    );
  }
}
