import '../../../person/domain/entities/person_entity.dart';

abstract class PersonEvent {}

class GetAllPersonEvent implements PersonEvent {}

class GetPersonEvent implements PersonEvent {
  final String id;

  GetPersonEvent(this.id);
}

class RegisterPersonEvent implements PersonEvent {
  final PersonEntity personEntity;

  RegisterPersonEvent(this.personEntity);
}
class UpdatePersonEvent implements PersonEvent {
  final PersonEntity personEntity;

  UpdatePersonEvent(this.personEntity);
}
class RemovePersonEvent implements PersonEvent {
  final PersonEntity personEntity;

  RemovePersonEvent(this.personEntity);
}
