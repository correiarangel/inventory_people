import '../../../person/domain/entities/person_entity.dart';


abstract class PersonState {}

class PersonInitiState implements PersonState {}

class LoadingPersonState implements PersonState {}

class LoadedPersonState implements PersonState {
  final List<PersonEntity> personList;

  LoadedPersonState(this.personList);
}

class ExceptionPersonState implements PersonState {
  final String mesage;

  ExceptionPersonState(this.mesage);
}
