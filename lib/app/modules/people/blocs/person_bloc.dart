import 'package:bloc_concurrency/bloc_concurrency.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'state/person_state.dart';

import '../../person/domain/usecases/delete_person_usecase.dart';
import '../../person/domain/usecases/get_all_person_usecase.dart';
import '../../person/domain/usecases/get_person_usecase.dart';
import '../../person/domain/usecases/register_person_usecase.dart';
import '../../person/domain/usecases/update_person.dart';
import 'event/person_event.dart';

class PersonBloc extends Bloc<PersonEvent, PersonState> {
  final IGetPersonUsecase _getPersonUsecase;
  final IGetAllPersonUsecase _getAllPersonUsecase;
  final IRegisterPersonUsecase _registerPersonUsecase;
  final IDeletePersonUsecase _removePersonUsecase;
  final IUpdatePersonUsecase _updatePersonUsecase;

  PersonBloc(
    this._getAllPersonUsecase,
    this._getPersonUsecase,
    this._registerPersonUsecase,
    this._updatePersonUsecase,
    this._removePersonUsecase,
  ) : super(PersonInitiState()) {
    on<GetPersonEvent>(_getPerson, transformer: restartable());
    on<GetAllPersonEvent>(_getAllPerson, transformer: restartable());
    on<RegisterPersonEvent>(_registerPerson, transformer: sequential());
    on<UpdatePersonEvent>(_updatePerson, transformer: sequential());
    on<RemovePersonEvent>(_removePerson, transformer: droppable());
  }

  Future<void> _removePerson(
      RemovePersonEvent event, Emitter<PersonState> emit) async {
    await _removePersonUsecase.call(event.personEntity.id);
  }

  Future<void> _updatePerson(
      UpdatePersonEvent event, Emitter<PersonState> emit) async {
    final _result = await _updatePersonUsecase.call(
        person: event.personEntity, id: event.personEntity.id);
    _result.fold((l) {

      return emit(ExceptionPersonState(l.message));
    }, (r) {
      r;
    });
  }

  Future<void> _registerPerson(
      RegisterPersonEvent event, Emitter<PersonState> emit) async {
    final _result = await _registerPersonUsecase.call(event.personEntity);
    _result.fold((l) {
      return emit(ExceptionPersonState(l.message));
    }, (r) {
      r;
    });
  }

  Future<void> _getPerson(GetPersonEvent event, Emitter emit) async {
    final result = await _getPersonUsecase.call(event.id);

    result.fold((l) {
      emit(ExceptionPersonState(l.message));
    }, (r) {});
  }

  Future<void> _getAllPerson(event, Emitter emit) async {
    emit(LoadingPersonState());
    final result = await _getAllPersonUsecase.call();

    result.fold((l) {
      emit(ExceptionPersonState(l.message));
    }, (r) {
      emit(LoadedPersonState(r));
    });
  }
}
