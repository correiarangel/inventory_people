abstract class IPersonException implements Exception {
  final String message;
  final StackTrace? stackTrace;
  const IPersonException({required this.message, required this.stackTrace});
}

class PersonError extends IPersonException {
  const PersonError({
    required String message,
    required StackTrace? stackTrace,
  }) : super(message: message, stackTrace: stackTrace);
}

