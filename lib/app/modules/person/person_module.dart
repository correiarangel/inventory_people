

import 'package:flutter_modular/flutter_modular.dart';
import 'domain/repositories/i_person_repository.dart';
import 'domain/usecases/delete_person_usecase.dart';
import 'domain/usecases/get_all_person_usecase.dart';
import 'domain/usecases/get_person_usecase.dart';
import 'domain/usecases/register_person_usecase.dart';
import 'domain/usecases/update_person.dart';
import 'external/person_datasource.dart';
import 'infra/datasources/i_person_datasource.dart';
import 'infra/repositories/person_reposytory.dart';

class PersonModule extends Module {
  @override
  final List<Bind> binds = [
    Bind<IPersonDatasource>(((i) => PersonDatasource(i.get())),export: true),
    Bind<IPersonRepository>(((i) => Personrepository(i.get())),export: true),
    Bind<IGetAllPersonUsecase>(((i) => GetAllPersonUsecase(i.get())),export: true),
    Bind<IGetPersonUsecase>(((i) => GetPersonUsecase(i.get())),export: true),
    Bind<IRegisterPersonUsecase>(((i) => RegisterPersonUsecase(i.get())),export: true),
    Bind<IDeletePersonUsecase>(((i) => DeletePersonUsecase(i.get())),export: true),
    Bind<IUpdatePersonUsecase>(((i) => UpdatePersonUsecase(i.get())),export: true),
  ];
}
