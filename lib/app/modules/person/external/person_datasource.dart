import '../../../core/shared/services/client_http/i_client_http.dart';
import '../failures/person_errors.dart';
import '../infra/datasources/i_person_datasource.dart';

class PersonDatasource implements IPersonDatasource {
  final IClientHttp client;

  PersonDatasource(this.client);
  @override
  Future<dynamic> delete({required String url}) async {
    final response = await client.delete(url);

    if (response.statusCode != 201) {
      return PersonError(
        message: response.statusCode.toString(),
        stackTrace: StackTrace.current,
      );
    }
    return true;
  }

  @override
  Future<dynamic> get({required String url}) async {
    final response = await client.get(url);

    if (response.statusCode != 200) {
      return PersonError(
        message: response.statusCode.toString(),
        stackTrace: StackTrace.current,
      );
    }
    return response.data;
  }

  @override
  Future<dynamic> register({
    required String url,
    required Map<String, dynamic> paramsMap,
  }) async {
    final response = await client.post(
      url,
      data: paramsMap,
    );

    if (response.statusCode != 201) {
      return PersonError(
        message: response.statusCode.toString(),
        stackTrace: StackTrace.current,
      );
    }
    return true;
  }

  @override
  Future<dynamic> update({
    required Map<String, dynamic> paramsMap,
    required String url,
  }) async {
    final response = await client.put(url, data: paramsMap);

    if (response.statusCode != 200) {
      return PersonError(
        message: response.statusCode.toString(),
        stackTrace: StackTrace.current,
      );
    }
    return true;
  }

  @override
  Future<dynamic> getAll({required String url}) async {
    final response = await client.get(url);

    if (response.statusCode != 200) {
      return PersonError(
        message: response.statusCode.toString(),
        stackTrace: StackTrace.current,
      );
    }

    return response.data;
  }
}
