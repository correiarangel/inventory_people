import '../../../../core/shared/types/either.dart';
import '../../failures/person_errors.dart';
import '../entities/person_entity.dart';
import '../repositories/i_person_repository.dart';

abstract class IGetPersonUsecase {
  Future<Either<IPersonException, PersonEntity>> call(String id);
}

class GetPersonUsecase extends IGetPersonUsecase {
  final IPersonRepository _repository;

  GetPersonUsecase(this._repository);
  @override
  Future<Either<IPersonException, PersonEntity>> call(String id) async {
    if (id.isEmpty) {
      return left(
        const PersonError(
          message: 'ERRO parametro ',
          stackTrace: StackTrace.empty,
        ),
      );
    }

    return await _repository.getPerson(id);
  }
}
