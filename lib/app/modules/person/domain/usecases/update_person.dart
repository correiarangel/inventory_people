import '../../../../core/shared/types/either.dart';
import '../../failures/person_errors.dart';
import '../entities/person_entity.dart';
import '../repositories/i_person_repository.dart';

abstract class IUpdatePersonUsecase {
  Future<Either<IPersonException, bool>> call({
    required PersonEntity person,
    required String id,
  });
}

class UpdatePersonUsecase extends IUpdatePersonUsecase {
  final IPersonRepository _repository;

  UpdatePersonUsecase(this._repository);
  @override
  Future<Either<IPersonException, bool>> call({
    required PersonEntity person,
    required String id,
  }) async {
    if (person.name.isEmpty || id.isEmpty) {
      return left(
        const PersonError(
          message: 'ERROR: name esta vazio',
          stackTrace: null,
        ),
      );
    }
    return await _repository.updatePerson(
      person: person,
      id: id,
    );
  }
}
