import '../../../../core/shared/types/either.dart';
import '../../failures/person_errors.dart';
import '../entities/person_entity.dart';
import '../repositories/i_person_repository.dart';

abstract class IRegisterPersonUsecase {
  Future<Either<IPersonException, bool>> call(PersonEntity person);
}

class RegisterPersonUsecase extends IRegisterPersonUsecase {
  final IPersonRepository _repository;

  RegisterPersonUsecase(this._repository);
  @override
  Future<Either<IPersonException, bool>> call(PersonEntity person) async {
    if (person.name.isEmpty) {
      return left(
        const PersonError(
          message: 'ERROR: name esta vazio',
          stackTrace: StackTrace.empty,
        ),
      );
    }
    return await _repository.registerPerson(person: person);
  }
}
