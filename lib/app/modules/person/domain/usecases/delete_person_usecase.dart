import '../../../../core/shared/types/either.dart';
import '../../failures/person_errors.dart';
import '../repositories/i_person_repository.dart';

abstract class IDeletePersonUsecase {
  Future<Either<IPersonException, bool>> call(String id);
}

class DeletePersonUsecase extends IDeletePersonUsecase {
  final IPersonRepository _repository;

  DeletePersonUsecase(this._repository);
  @override
  Future<Either<IPersonException, bool>> call(String id) async {
    if (id.isEmpty) {
      return  left(
        const PersonError(
          message: 'ERRO parametro vazio',
          stackTrace: null,
        ),
      );
    }
    return await _repository.deletePerson(id);
  }
}
