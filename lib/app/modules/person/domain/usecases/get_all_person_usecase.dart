import '../../../../core/shared/types/either.dart';
import '../../failures/person_errors.dart';
import '../entities/person_entity.dart';

import '../repositories/i_person_repository.dart';

abstract class IGetAllPersonUsecase {
  Future<Either<IPersonException, List<PersonEntity>>> call();
}

class GetAllPersonUsecase extends IGetAllPersonUsecase {
  final IPersonRepository _repository;

  GetAllPersonUsecase(this._repository);
  @override
  Future<Either<IPersonException, List<PersonEntity>>> call() async {
    final result = await _repository.getAllPerson();
    if (result.isLeft) {
      return left(
        const PersonError(
          message: '',
          stackTrace: null,
        ),
      );
    }

    return result;
  }
}
