import '../value_objects/type_profiles.dart';

class PersonEntity {
  final String id;
  final String name;
  final String cpf;
    final String email;
  final Profiles profiles;
  

  PersonEntity({
    required this.id,
    required this.name,
    required this.email,
    required this.cpf,
    required this.profiles,
  });
}
