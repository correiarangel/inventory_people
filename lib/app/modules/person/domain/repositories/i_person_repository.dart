import '../../../../core/shared/types/either.dart';
import '../../failures/person_errors.dart';
import '../entities/person_entity.dart';

abstract class IPersonRepository {
  Future<Either<IPersonException, List<PersonEntity>>> getAllPerson();
  Future<Either<IPersonException, bool>> registerPerson(
      {required PersonEntity person});
  Future<Either<IPersonException, bool>> updatePerson({
    required PersonEntity person,
    required String id,
  });
  Future<Either<IPersonException, PersonEntity>> getPerson(String id);
  Future<Either<IPersonException, bool>> deletePerson(String id);
}
