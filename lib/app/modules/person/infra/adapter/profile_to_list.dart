import '../../domain/value_objects/type_profiles.dart';

class ProfileTolist {
  // ignore: body_might_complete_normally_nullable
  static Profiles? convertMapToProfilesENUM(Map<String, dynamic> map) {
    final list = map['profiles'] as List;
    if (list[0] == 'USER') {
      return Profiles.user;
    } else if (list[0] == 'MANAGER') {
      return Profiles.manager;
    } else if (list[0] == 'ADMINISTRATOR') {
      return Profiles.administrator;
    }
    
  }
}
