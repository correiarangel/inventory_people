
import '../../domain/entities/person_entity.dart';

class PersonToJsonAdapter {
 static Map<String, dynamic> toMap(PersonEntity person) {
    return {
      'id': person.id,
      'name': person.name,
      'cpf': person.cpf,
      'email':person.email,
      'profiles': [person.profiles.name.toUpperCase()],
    };
  }

}
