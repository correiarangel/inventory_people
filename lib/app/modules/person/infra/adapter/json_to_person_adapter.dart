import 'profile_to_list.dart';

import '../../domain/entities/person_entity.dart';

class JsonToPersonAdapter {
  static PersonEntity fromMap(Map<String, dynamic> map) {
    return PersonEntity(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
      cpf: map['cpf'] ?? '',
      email: map['email'] ?? '',
      profiles: ProfileTolist.convertMapToProfilesENUM(map)!,
    );
  }
}
