import '../adapter/json_to_person_adapter.dart';

import '../../../../core/shared/value/const_string_url.dart';

import '../../../../core/shared/types/either.dart';
import '../../domain/entities/person_entity.dart';
import '../../domain/repositories/i_person_repository.dart';
import '../../failures/person_errors.dart';

import '../adapter/person_to_json_adapter.dart';
import '../datasources/i_person_datasource.dart';

class Personrepository implements IPersonRepository {
  final IPersonDatasource _datasource;

  Personrepository(this._datasource);

  @override
  Future<Either<IPersonException, List<PersonEntity>>> getAllPerson() async {
    final result = await _datasource.getAll(url: ConstStringUrl.urlBase);

    if (result is PersonError) {
      return left(
        PersonError(
          message: 'Não ouve retorno de pessoas!',
          stackTrace: StackTrace.current,
        ),
      );
    }

    final mapResp = result as List;
    final listPerson =
        mapResp.map((map) => JsonToPersonAdapter.fromMap(map)).toList();
    return right(listPerson);
  }

  @override
  Future<Either<IPersonException, bool>> registerPerson(
      {required PersonEntity person}) async {
    final paramsMap = PersonToJsonAdapter.toMap(person);
    paramsMap.remove('id');

    final result = await _datasource.register(
        paramsMap: paramsMap, url: ConstStringUrl.urlBase);

    if (result is PersonError) {
      return left(
        PersonError(
          message: 'Não ouve retorno de pessoas!',
          stackTrace: StackTrace.current,
        ),
      );
    }

    return right(true);
  }

  @override
  Future<Either<PersonError, bool>> updatePerson({
    required PersonEntity person,
    required String id,
  }) async {
    final paramsMap = PersonToJsonAdapter.toMap(person);

    paramsMap.remove('id');
    final url = '${ConstStringUrl.urlBase}$id';

    final result = await _datasource.update(
      paramsMap: paramsMap,
      url: url,
    );

    if (result is PersonError) {
      return left(PersonError(
        message: 'Erro: pessoa não foi atulisada',
        stackTrace: StackTrace.current,
      ));
    }
    return right(true);
  }

  @override
  Future<Either<IPersonException, PersonEntity>> getPerson(String id) async {
    var url = '${ConstStringUrl.urlBase}/$id';
    final result = await _datasource.get(url: url);
    if (result is PersonError) {
      return left(PersonError(
        message: 'Erro: pessoa não foi atulisada',
        stackTrace: StackTrace.current,
      ));
    }
    return right(JsonToPersonAdapter.fromMap(result));
  }

  @override
  Future<Either<IPersonException, bool>> deletePerson(String id) async {
    final url = '${ConstStringUrl.urlBase}/$id';

    final result = await _datasource.delete(url: url);
    if (result is bool) {
      return right(true);
    }
    return left(
      const PersonError(
        message: "ERRO  Pessoa não foi deletada",
        stackTrace: null,
      ),
    );
  }
}
