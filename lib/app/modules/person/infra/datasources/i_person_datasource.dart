
abstract class IPersonDatasource {
  Future<dynamic> getAll({required String url});
  Future<dynamic> get({required String url});
  Future<dynamic> delete({required String url});
  Future<dynamic> update({required Map<String, dynamic> paramsMap,required String url});

  Future<dynamic> register({
    required String url,
    required Map<String, dynamic> paramsMap,
  });
}
