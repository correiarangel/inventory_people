import 'package:flutter_modular/flutter_modular.dart';
import 'package:inventory_people/app/modules/register/presenter/controllers/person_controller.dart';
import 'presenter/pages/register_page.dart';

class RegisterModule extends Module {
  @override
  final List<Bind> binds = [
    Bind(((i) => PersonController())),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/register', child: (_, args) => const RegisterPage()),
  ];
}
