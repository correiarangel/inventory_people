import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../controllers/person_controller.dart';
import '../../../../core/shared/components/buttom_action.dart';
import '../../../../core/shared/components/input_field.dart';
import '../../../../core/todo_ui/const_colors.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final List<DropdownMenuItem<String>> _listaItensDrop = [
    const DropdownMenuItem(child: Text('Usuario'), value: 'USER'),
    const DropdownMenuItem(child: Text('Gerente'), value: 'MANAGER'),
    const DropdownMenuItem(child: Text('Adminitrador'), value: 'ADMINISTRATOR'),
  ];

  final _controller = Modular.get<PersonController>();
  dynamic _itemSelecionado;
  @override
  Widget build(BuildContext context) {
    //final height = MediaQuery.of(context).size.height / 100;
    //final width = MediaQuery.of(context).size.width / 100;
    return Scaffold(
      appBar: AppBar(
        actions: [
          const Text('Home'),
          const Spacer(),
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.logout_outlined,
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: [
            const SizedBox(
              height: 32.0,
            ),
            InputField(
                controllerText: _controller.controllerName,
                icon: Icons.person_add_alt_1_rounded,
                hint: 'Digite nome',
                labelText: 'Nome:',
                obscureText: false),
            const SizedBox(
              height: 16.0,
            ),
            InputField(
              controllerText: _controller.controllerCPF,
              icon: Icons.admin_panel_settings_sharp,
              hint: 'Digite CPF',
              labelText: 'CPF:',
              obscureText: false,
            ),
            const SizedBox(
              height: 16.0,
            ),
            InputField(
              controllerText: _controller.controllerPasswod,
              icon: Icons.password_rounded,
              hint: 'Digite uma senha',
              labelText: 'Senha:',
              obscureText: false,
            ),
            const SizedBox(
              height: 16.0,
            ),
            InputField(
              controllerText: _controller.controllerPasswod,
              icon: Icons.password_rounded,
              hint: 'Confirme a senha',
              labelText: 'Confirmar:',
              obscureText: false,
            ),
            const SizedBox(
              height: 16.0,
            ),
            DropdownButtonFormField(
              value: _itemSelecionado,
              hint: const Text("Selecione um tipo"),
              style: const TextStyle(
                color: ConstColors.colorBlackOlive,
                fontSize: 16,
              ),
              items: _listaItensDrop,
              onChanged: (value) {
                setState(() => _itemSelecionado = value);
              },
            ),
            const SizedBox(
              height: 16.0,
            ),
            ButtonAction(textSize: 24, text: 'SALVAR', callback: () {},),
            const SizedBox(
              height: 16.0,
            ),
            ButtonAction(textSize: 24, text: 'CANCELAR', callback: () {},),
          ],
        ),
      ),
    );
  }
}
