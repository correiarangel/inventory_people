
import '../../failures/login_errors.dart';
import '../../../../core/shared/types/either.dart';
import '../../presenter/models/current_user_model.dart';

abstract class ILoginRepository {
  Future<Either<LoginError, CurrentUserModel>> login({
    required String email,
    required String password,
  });
}
