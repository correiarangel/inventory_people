import '../../failures/login_errors.dart';
import '../entities/currenty_user_entity.dart';

import '../../../../core/shared/types/either.dart';
import '../repositories/i_login_repository.dart';

abstract class ILoginUsecase {
  Future<Either<LoginError, CurrentUserEnptity>> call({
    required String email,
    required String password,
  });
}

class LoginUsecase extends ILoginUsecase {
  final ILoginRepository _repository;

  LoginUsecase(this._repository);

  @override
  Future<Either<LoginError, CurrentUserEnptity>> call({
    required String email,
    required String password,
  }) async {
    if (email.isEmpty || password.isEmpty) {
      return left(
        const LoginError(
          message: 'ERROR: Invalid email or password',
          stackTrace: null,
        ),
      );
    } else if (password.length < 10) {
      return left(
        const LoginError(
          message: 'ERROR: password must contain 10 characters',
          stackTrace: null,
        ),
      );
    } else if (email.length < 5) {
      return left(
        const LoginError(
          message: 'ERROR: too small for an email ',
          stackTrace: null,
        ),
      );
    } else if (!email.contains('@')) {
      return left(
        const LoginError(
          message: 'ERROR:email must contain @',
          stackTrace: null,
        ),
      );
    }
    return await _repository.login(email: email, password: password);
  }
}
