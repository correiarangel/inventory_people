import '../../presenter/models/current_user_model.dart';

class CurrentUserToJson {
  static Map<String, dynamic> toMap(CurrentUserModel user) {
    return {
      'userEmail': user.userEmail,
      'tokenType': user.tokenType,
      'token': user.token,
    };
  }

  static CurrentUserModel fromMap(Map<String, dynamic> map) {
    return CurrentUserModel(
      userEmail: map['userEmail'],
      tokenType: map['tokenType'],
      token: map['token'],
    );
  }
}
