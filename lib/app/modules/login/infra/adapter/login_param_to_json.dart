class LoginParamToJson {
  static Map<String, dynamic> toMap({
    required String email,
    required String password,
  }) {
    return {
      'username': email,
      'password': password,
    };
  }
}
