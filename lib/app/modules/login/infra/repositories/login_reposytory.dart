import 'package:flutter_modular/flutter_modular.dart';
import 'package:inventory_people/app/app_module.dart';
import '../../../../core/shared/services/shared_preference/i_local_storege_service.dart';
import '../../../../core/shared/value/const_string_url.dart';
import '../../value_object/storege_params_type.dart';

import '../../../../core/shared/types/either.dart';
import '../../domain/repositories/i_login_repository.dart';
import '../../failures/login_errors.dart';
import '../../presenter/models/current_user_model.dart';
import '../adapter/current_user_to_json.dart';
import '../adapter/login_param_to_json.dart';
import '../datasources/i_login_datasource.dart';

class LoginRepository implements ILoginRepository {
  final ILoginDatasource _datasource;

  LoginRepository(this._datasource);

  @override
  Future<Either<LoginError, CurrentUserModel>> login({
    required String email,
    required String password,
  }) async {
    try {
      final mapData = LoginParamToJson.toMap(
        email: email,
        password: password,
      );
      final result = await _datasource.login(mapData: mapData);

      final userModel = CurrentUserToJson.fromMap(result);
      setToken(userModel.token);
      return right(userModel);
    } on LoginError catch (e, s) {
      return left(
        LoginError(
          message: e.toString(),
          stackTrace: s,
        ),
      );
    } catch (e) {
      return left(
        LoginError(
          message: e.toString(),
          stackTrace: StackTrace.current,
        ),
      );
    }
  }

  Future<void> setToken(String token) async {
    await Modular.isModuleReady<AppModule>();

    final localStorage = Modular.get<ILocalStorage>();
    await localStorage.setData(
        params: StoregeParamsType(
      key: ConstStringUrl.jWToken,
      value: token,
    ));
  }


}
