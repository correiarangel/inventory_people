import 'package:flutter/cupertino.dart';

@immutable
class ParamLoginModel {
  final String email;
  final String password;
  const ParamLoginModel({
    required this.email,
    required this.password,
  });

  factory ParamLoginModel.empty() {
    return const ParamLoginModel(
      email: '',
      password: '',
    );
  }
  ParamLoginModel copyWith({
    String? email,
    String? password,
  }) {
    return ParamLoginModel(
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }
}
