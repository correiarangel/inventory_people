import '../../domain/entities/currenty_user_entity.dart';

class CurrentUserModel extends CurrentUserEnptity {
  CurrentUserModel({
    required String userEmail,
    required String tokenType,
    required String token,
  }) : super(
          userEmail: userEmail,
          tokenType: tokenType,
          token: token,
  );

  CurrentUserModel copyWith({
    String? userEmail,
    String? tokenType,
    String? token,
  }) {
    return CurrentUserModel(
      userEmail: userEmail ?? this.userEmail,
      tokenType: tokenType ?? this.tokenType,
      token: token ?? this.token,
    );
  }

  factory CurrentUserModel.empty() {
    return CurrentUserModel(
      userEmail: '',
      tokenType: '',
      token: '',
    );
  }
}
