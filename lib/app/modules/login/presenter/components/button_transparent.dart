import 'package:flutter/material.dart';

class ButtonTransparent extends StatelessWidget {
  final VoidCallback callback;
  final String text;
  final double marginLeft;
  final double marginRight;
  final double height;
  final double? width;
  final double fontSize;
  final Color borderColor;
  final Color textColor;

  const ButtonTransparent(
      {required this.callback,
      required this.text,
      required this.marginLeft,
      required this.marginRight,
      required this.height,
      this.width,
      required this.fontSize,
      required this.borderColor,
      required this.textColor,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        margin: EdgeInsets.only(top: 5, left: marginLeft, right: marginRight),
        alignment: Alignment.center,
        height: height,
        width: width,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.vertical(
                top: Radius.circular(8), bottom: Radius.circular(8)),
            color: Colors.transparent,
            border: Border.all(width: 1, color: borderColor)),
        child: Text(
          text,
          style: TextStyle(
              color: textColor,
              fontWeight: FontWeight.w400,
              fontSize: fontSize),
        ),
      ),
    );
  }
}
