import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../blocs/event/login_params_event.dart';
import '../blocs/login_paramas_bloc.dart';
import '../blocs/state/login_params_state.dart';
import '../models/param_login_model.dart';

import '../../../../core/todo_ui/const_colors.dart';

class TextFormFieldCustom extends StatelessWidget {
  const TextFormFieldCustom({Key? key}) : super(key: key);
  String? _changeValueEmail(String? value) {
    if (value == null || value.isEmpty) return 'E-mail requerido...';
    return null;
  }

  String? _changeValuePassword(String? value) {
    if (value == null || value.isEmpty) return 'Senha requerida...';
    return null;
  }
/*
  //late StreamSubscription subLogin;
  @override
  void initState() {

    super.initState();
    subLogin = widget.loginBloc.stream.listen((state) async {
      if (state is AuthSuccessState) {

        await widget.localStorage.save(
            ParamShered(key: 'token', value: state.currentUserModel.token));
      }
      if (state is AuthErrorState) {

      }
    }); 
  }

  @override
  void dispose() {
    // subLogin.cancel();
    widget.loginBloc.close();
    super.dispose();
  }

*/
  @override
  Widget build(BuildContext context) {
    final _bloc = Modular.get<LoginParamsBloc>();
    ParamLoginModel _paramsModel = ParamLoginModel.empty();

    return BlocBuilder<LoginParamsBloc,ParamsLoginState>(
      bloc: _bloc,
      builder: (_, state) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              child: TextFormField(
                 validator: (value) {
                  if (state is ParamsLoginErroState) {
                    return state.mesage;
                  }
                  return _changeValueEmail(value);
                }, 
                
                onChanged: (value) {
                  debugPrint('Digitou email $value');
                  _paramsModel = _paramsModel.copyWith(email: value);
                  _bloc.add(ParamsLoginValidatedEvent(_paramsModel));
                },
                obscureText: false,
                decoration: const InputDecoration(
                  fillColor: ConstColors.colorPrimaryMardiGras,
                  icon: Icon(
                    Icons.email_rounded,
                    color: ConstColors.colorPrimaryMardiGras,
                  ),
                  hintText: 'Digite e-mail',
                  labelText: 'E-mail:',
                  labelStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  hintStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: ConstColors.colorPrimaryMardiGras,
                    ),
                  ),
                  contentPadding: EdgeInsets.only(
                    bottom: 10.0,
                    top: 20.0,
                    right: 20.0,
                    left: 5.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              child: TextFormField(
                 validator: (value) {
                  if (state is ParamsLoginErroState) {
                    return state.mesage;
                  }
                  return _changeValuePassword(value);
                }, 
                onChanged: (value) {
                  debugPrint('Digitou senha $value');
                  _paramsModel = _paramsModel.copyWith(password: value);
                _bloc.add(ParamsLoginValidatedEvent(_paramsModel));
                },
                obscureText: false,
                decoration: const InputDecoration(
                  fillColor: ConstColors.colorPrimaryMardiGras,
                  icon: Icon(
                    Icons.https_rounded,
                    color: ConstColors.colorPrimaryMardiGras,
                  ),
                  hintText: 'Digite sua senha',
                  labelText: 'Senha:',
                  labelStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  hintStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: ConstColors.colorPrimaryMardiGras,
                    ),
                  ),
                  contentPadding: EdgeInsets.only(
                    bottom: 10.0,
                    top: 20.0,
                    right: 20.0,
                    left: 5.0,
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
