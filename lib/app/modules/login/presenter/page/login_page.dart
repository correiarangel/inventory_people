import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:inventory_people/app/app_module.dart';

import '../../../../core/shared/components/buttom_action.dart';
import '../../../../core/shared/services/overlay/asuka_overlay_service.dart';
import '../../../../core/todo_ui/const_colors.dart';
import '../../../person/infra/repositories/person_reposytory.dart';
import '../blocs/event/login_event.dart';
import '../blocs/login_bloc.dart';
import '../blocs/state/login_state.dart';
import '../components/button_transparent.dart';
import '../models/param_login_model.dart';

class LoginPage extends StatefulWidget {
  final LoginBloc loginBloc;

  const LoginPage({
    Key? key,
    required this.loginBloc,
  }) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPagesState();
}

class _LoginPagesState extends State<LoginPage> {
  // final _controller = Modular.get<LoginController>();
  final mesage = Modular.get<AsukaOverlayService>();

  late ParamLoginModel _paramsModel;

  @override
  void dispose() {
    widget.loginBloc.close();
    super.dispose();
  }

  test() async {
    await Modular.isModuleReady<AppModule>();
    final repo = Modular.get<Personrepository>();
    final res = await repo.getAllPerson();
    if (res.isRight) Modular.to.pushReplacementNamed('/home');
  }

  @override
  void initState() {
    test();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height / 100;
    //final width = MediaQuery.of(context).size.width / 100;
    _paramsModel = ParamLoginModel.empty();

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: height * 20),
            CircleAvatar(
              radius: 50.0,
              backgroundColor: ConstColors.colorPrimaryMardiGras,
              child: Image.asset('images/logocirculo.png'),
            ),
            SizedBox(height: height * 10),
            Padding(
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              child: TextFormField(
                onChanged: (value) {
                  debugPrint('Digitou email $value');
                  _paramsModel = _paramsModel.copyWith(email: value);
                },
                obscureText: false,
                decoration: const InputDecoration(
                  fillColor: ConstColors.colorPrimaryMardiGras,
                  icon: Icon(
                    Icons.email_rounded,
                    color: ConstColors.colorPrimaryMardiGras,
                  ),
                  hintText: 'Digite e-mail',
                  labelText: 'E-mail:',
                  labelStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  hintStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: ConstColors.colorPrimaryMardiGras,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              child: TextFormField(
                onChanged: (value) {
                  debugPrint('Digitou senha $value');
                  _paramsModel = _paramsModel.copyWith(password: value);
                },
                obscureText: false,
                decoration: const InputDecoration(
                  fillColor: ConstColors.colorPrimaryMardiGras,
                  icon: Icon(
                    Icons.https_rounded,
                    color: ConstColors.colorPrimaryMardiGras,
                  ),
                  hintText: 'Digite sua senha',
                  labelText: 'Senha:',
                  labelStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  hintStyle: TextStyle(
                    color: ConstColors.colorBlackOlive,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: ConstColors.colorPrimaryMardiGras,
                    ),
                  ),
                ),
              ),
            ),
            BlocBuilder<LoginBloc, AuthState>(
              bloc: widget.loginBloc,
              builder: (_, state) {
                if (state is AuthErrorState) {
                  debugPrint(state.appException.message);
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    debugPrint(state.appException.message);
                    mesage.showSnackBar(Text(state.appException.message));
                  });
                }

                if (state is AuthSuccessState) {
                  Modular.to.pushReplacementNamed('/home');
                  WidgetsBinding.instance.addPostFrameCallback((_) async {
                    mesage.showSnackBar(const Text('Logado com Sucesso !'));
                  });
                  _paramsModel = ParamLoginModel.empty();
                }

                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Visibility(
                      visible: state is AuthLoadingState ? true : false,
                      child: const Padding(
                        padding: EdgeInsets.all(32.0),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
                    const SizedBox(height: 32.0),
                    Visibility(
                      visible: state is AuthLoadingState ? false : true,
                      child: ButtonAction(
                        textSize: 24,
                        text: 'ENTRAR',
                        callback: () {
                          widget.loginBloc.add(LoginEvent(_paramsModel));
                        },
                      ),
                    ),
                  ],
                );
              },
            ),
            const SizedBox(height: 16.0),
            ButtonTransparent(
              callback: () {
                Modular.to.pushNamed('/register');
              },
              text: 'CADASTRAR SE',
              marginLeft: 16,
              marginRight: 16,
              height: 48,
              fontSize: 24,
              borderColor: ConstColors.colorPrimaryMardiGras,
              textColor: ConstColors.colorPrimaryMardiGras,
            ),
          ],
        ),
      ),
    );
  }
}
