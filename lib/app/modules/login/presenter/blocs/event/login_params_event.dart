import '../../models/param_login_model.dart';

abstract class ParamsLoginEvent {}

class ParamsLoginValidatedEvent extends ParamsLoginEvent {
  final ParamLoginModel _paramLoginModel;

  ParamsLoginValidatedEvent(this._paramLoginModel);
  ParamLoginModel get paramLoginModel => _paramLoginModel;
}
