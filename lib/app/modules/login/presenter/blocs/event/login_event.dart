import 'package:flutter/cupertino.dart';

import '../../models/param_login_model.dart';

@immutable
abstract class AuthEvent {}

class LoginEvent extends AuthEvent {
  final ParamLoginModel _paramLoginModel;

  LoginEvent(this._paramLoginModel);
  ParamLoginModel get paramLoginModel => _paramLoginModel;
}


