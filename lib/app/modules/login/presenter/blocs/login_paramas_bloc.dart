import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'event/login_params_event.dart';
import 'state/login_params_state.dart';

class LoginParamsBloc extends Bloc<ParamsLoginEvent, ParamsLoginState> {
  LoginParamsBloc() : super(ParamsLoginInitState()) {
    on<ParamsLoginValidatedEvent>(_changesParams);
  }

  Future<void> _changesParams(
    ParamsLoginValidatedEvent event,
    Emitter emit,
  ) async {
    emit(ParamsLoginInitState());
    final valid = _validParams(event);
    debugPrint('Validaou --->  $valid');
    if (valid.length > 1) {
      emit(ParamsLoginErroState(mesage: valid));
    }

    if (valid.isEmpty) {
      emit(ParamsLoginSuccessState(event.paramLoginModel));
    }
  }

  String _validParams(ParamsLoginValidatedEvent event) {
    final params = event.paramLoginModel;
    if (params.email.isEmpty) return 'E-mail deve ser informado';
    if (params.email.contains('@')) return 'E-mail deve @';
    if (params.password.isEmpty) return 'Senha deve ser informado';
    if (params.password.length < 8) return 'Senha deve ser informado';
    return '';
  }
}
