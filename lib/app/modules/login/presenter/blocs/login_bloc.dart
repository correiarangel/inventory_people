import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../models/current_user_model.dart';
import 'event/login_event.dart';
import 'state/login_state.dart';
import '../../domain/usecases/login_usecase.dart';

class LoginBloc extends Bloc<AuthEvent, AuthState> {
  final ILoginUsecase _usecase;

  LoginBloc(this._usecase) : super(AuthInitialState()) {
    on<LoginEvent>(_login);
    // on<LogOutEvent>(_logOut);
  }

  Future<void> _login(LoginEvent event, Emitter emit) async {
    emit(AuthLoadingState());

    final currentUserEnptity = await _usecase.call(
      email: event.paramLoginModel.email,
      password: event.paramLoginModel.password,
    );
    /*     currentUserEnptity.fold(
      (error) {
        emit(AuthErrorState(LoginError(
            message: error.message, stackTrace: StackTrace.current)));
      },
      (currentUserModel) {
        emit(AuthSuccessState(currentUserModel));
      },
    );
    */
    currentUserEnptity.fold((l) {
      emit(AuthErrorState(l));
    }, (r) {
      debugPrint('DEU BOM !!!!!!  SAVE CurrentUser ........${r.userEmail}');
      r as CurrentUserModel;
      emit(AuthSuccessState(r));
    });
  }

/*   Future<void> _logOut(LogOutEvent event, Emitter emit) async {
    emit(AuthLoadingState());
    emit(AuthLogOutState());
  } */
}
