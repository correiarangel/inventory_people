import '../../models/param_login_model.dart';

abstract class ParamsLoginState {}

class ParamsLoginInitState implements ParamsLoginState {}

class ParamsLoginValidatedState implements ParamsLoginState {
  final ParamLoginModel paramsLoginModel;

  ParamsLoginValidatedState(this.paramsLoginModel);
}

class ParamsLoginSuccessState implements ParamsLoginState {
  final ParamLoginModel paramsLoginModel;

  ParamsLoginSuccessState(this.paramsLoginModel);
}



class ParamsLoginErroState implements ParamsLoginState {
  final String mesage;

  ParamsLoginErroState({required this.mesage});
}
