import '../../../failures/login_errors.dart';
import '../../models/current_user_model.dart';

abstract class AuthState {}

class AuthInitialState implements AuthState {}

class AuthLoadingState implements AuthState {}

class AuthSuccessState implements AuthState {
  final CurrentUserModel currentUserModel;

  AuthSuccessState(this.currentUserModel);
}

class AuthErrorState implements AuthState {
  final LoginError appException;

  AuthErrorState(this.appException);
}

