abstract class IAppException implements Exception {
  final String message;
  final StackTrace? stackTrace;
  const IAppException({required this.message, required this.stackTrace});
}


class LoginError extends IAppException {
  const LoginError({
    required String message,
    required StackTrace? stackTrace,
  }) : super(message: message, stackTrace: stackTrace);
}










