import 'package:flutter_modular/flutter_modular.dart';
import 'presenter/blocs/login_paramas_bloc.dart';
import 'presenter/page/login_page.dart';
import '../register/register_module.dart';
import 'package:modular_bloc_bind/modular_bloc_bind.dart';

import '../home/home_module.dart';
import 'domain/repositories/i_login_repository.dart';
import 'domain/usecases/login_usecase.dart';
import 'external/login_datasource.dart';
import 'infra/datasources/i_login_datasource.dart';
import 'infra/repositories/login_reposytory.dart';
import 'presenter/blocs/login_bloc.dart';
import 'presenter/controllers/login_controller.dart';

class LoginModule extends Module {

  
  @override
  final List<Bind> binds = [
    
    Bind.factory<ILoginDatasource>((i) => LoginDatasource(i()), export: true),
    
    Bind<ILoginRepository>((i) => LoginRepository(i()), export: true),
    Bind.factory<ILoginUsecase>((i) => LoginUsecase(i()), export: true),
    Bind((i) => LoginController(), export: true),
    BlocBind.singleton((i) => LoginBloc(i()), export: true),
    BlocBind.singleton((i) => LoginParamsBloc(), export: true),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(
      '/',
      child: (context, args) => LoginPage(

        loginBloc: Modular.get<LoginBloc>(),
      ),
    ),
    ModuleRoute('/', module: HomeModule()),
    ModuleRoute('/', module: RegisterModule()),
  ];
}
