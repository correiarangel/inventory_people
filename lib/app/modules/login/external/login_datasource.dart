import '../../../core/shared/services/client_http/i_client_http.dart';
import '../../../core/shared/value/const_string_url.dart';
import '../failures/login_errors.dart';
import '../infra/datasources/i_login_datasource.dart';

class LoginDatasource implements ILoginDatasource {
  final IClientHttp _clientHttpInterface;

  LoginDatasource(this._clientHttpInterface);

  @override
  Future<dynamic> login({required Map<String, dynamic> mapData}) async {
    final response =
        await _clientHttpInterface.loginPost(ConstStringUrl.urlLogin, data: mapData);

    if (response.statusCode == 200) {
      return response.data;
    }

    return LoginError(
      message: response.statusCode.toString(),
      stackTrace: StackTrace.empty,
    );
  }
}


