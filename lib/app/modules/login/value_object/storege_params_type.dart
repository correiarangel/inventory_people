import '../../../core/shared/services/shared_preference/shared_params.dart';

class StoregeParamsType extends SharedParams {
  StoregeParamsType({required String key, required value})
      : super(
          key: key,
          value: value,
        );
}
