import 'package:flutter_modular/flutter_modular.dart';
import 'presenter/pages/home_page.dart';

import '../people/people_module.dart';
import '../person/person_module.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute('/', module: PersonModule()),
    ModuleRoute('/', module: PeopleModule()),
    ChildRoute('/home', child: (context, args) => const HomePage()),
  ];
}
