import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/shared/components/buttom_action.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          const Padding(
            padding: EdgeInsets.only(top: 12, left: 24.0),
            child: Text(
              'Home',
              style: TextStyle(fontSize: 24.0),
            ),
          ),
          const Spacer(),
          IconButton(
            onPressed: () {
              // _blocLogin.add(LogOutEvent());
            },
            icon: const Icon(
              Icons.logout_outlined,
            ),
          ),
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        children: [
          const SizedBox(height: 32.0),
          ButtonAction(
              textSize: 24,
              text: 'Pessoas',
              callback: () {
                Modular.to.pushNamed('/people');
              })
        ],
      ),
    );
  }
}
