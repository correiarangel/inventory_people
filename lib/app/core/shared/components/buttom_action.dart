import 'package:flutter/material.dart';

import '../../todo_ui/const_colors.dart';

class ButtonAction extends StatelessWidget {
  const ButtonAction({
    Key? key,
    required this.textSize,
    required this.text,
    required this.callback,
  }) : super(key: key);

  final double textSize;
  final String text;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, left: 16, right: 16),
      child: ElevatedButton(
          child: Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: textSize),
          ),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              ConstColors.colorPrimaryMardiGras,
            ),
            elevation: MaterialStateProperty.all(9.0),
            padding: MaterialStateProperty.all(
                const EdgeInsets.fromLTRB(48, 12, 48, 12)),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            )),
          ),
          onPressed: callback),
    );
  }
}
