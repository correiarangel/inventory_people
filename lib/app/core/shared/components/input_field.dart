import 'package:flutter/material.dart';

import '../../todo_ui/const_colors.dart';

class InputField extends StatelessWidget {
  final IconData icon;
  final String hint;
  final String labelText;
  final bool obscureText;
  final TextEditingController controllerText;
  // final Stream<String> stream;
  //final dynamic onChanged;

  const InputField({
    Key? key,
    //required this.onChanged,
    required this.icon,
    required this.hint,
    required this.labelText,
    required this.obscureText, 
    required this.controllerText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: (value) {
        
      },
      controller: controllerText,
      obscureText: obscureText,
      decoration: InputDecoration(
        fillColor: ConstColors.colorPrimaryMardiGras,
        icon: Icon(
          icon,
          color: ConstColors.colorPrimaryMardiGras,
        ),
        hintText: hint,
        labelText: labelText,
        labelStyle: const TextStyle(
          color: ConstColors.colorBlackOlive,
        ),
        hintStyle: const TextStyle(
          color: ConstColors.colorBlackOlive,
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
            color: ConstColors.colorPrimaryMardiGras,
          ),
        ),
        contentPadding: const EdgeInsets.only(
          bottom: 10.0,
          top: 20.0,
          right: 20.0,
          left: 5.0,
        ),
      ),
    );
  }
}
