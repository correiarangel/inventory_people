class ConstStringUrl {
  // https://poc-person-service.herokuapp.com/poc/person-api/v1/person/
  static const String v = 'v1';
  static const String urlBase =
      'https://poc-person-service.herokuapp.com/poc/person-api/$v/person/';
  static const String urlLogin =
      'https://poc-person-service.herokuapp.com/poc/person-api/$v/authenticates/login';
  static const jWToken = 'JWToken';
}
