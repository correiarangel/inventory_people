import '../../failures/app_errors.dart';
import 'shared_params.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'i_local_storege_service.dart';

class SharedPreferencesService implements ILocalStorage {
  final SharedPreferences sharedPreferences;

  SharedPreferencesService(this.sharedPreferences);

  @override
  dynamic getData(String key) {
    final result = sharedPreferences.get(key);

    if (result != null) {
      return result;
    }

    throw SharedPreferencesException(
        'There is no key ($key) passed as a parameter');
  }

  @override
  Future<bool> setData({required SharedParams params}) async {
    switch (params.value.runtimeType) {
      case String:
        return await sharedPreferences.setString(params.key, params.value);
      case int:
        return await sharedPreferences.setInt(params.key, params.value);
      case bool:
        return await sharedPreferences.setBool(params.key, params.value);
      case double:
        return await sharedPreferences.setDouble(params.key, params.value);
      case List<String>:
        return await sharedPreferences.setStringList(params.key, params.value);
    }
    return false;
  }

  @override
  Future<bool> removeData(String key) async {
    return await sharedPreferences.remove(key);
  }
}
