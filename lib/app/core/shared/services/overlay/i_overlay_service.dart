import 'package:flutter/material.dart';

abstract class IOverlayService {
  void showSnackBar(Widget contentText);
}