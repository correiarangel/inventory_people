import 'package:flutter/material.dart';

import '../navigation_service/navigation_service.dart';
import 'i_overlay_service.dart';

/* class AsukaOverlayService extends IOverlayService {

  @override
  void showSnackBar(SnackBar widget) {
    asuka.showSnackBar(widget);
  }
} */

class AsukaOverlayService extends IOverlayService {

  @override
  void showSnackBar(Widget contentText) {
    late SnackBar snackBar = SnackBar(
      content: contentText,
    );

    ScaffoldMessenger.of(NavigationService.navigatorKey.currentContext!)
        .showSnackBar(snackBar);
  }
}
