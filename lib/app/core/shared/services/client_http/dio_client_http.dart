import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:inventory_people/app/app_module.dart';
import '../shared_preference/i_local_storege_service.dart';
import '../shared_preference/shared_params.dart';
import '../../value/const_string_url.dart';

import 'i_client_http.dart';

class DioClientHttp implements IClientHttp {
  final Dio _dio;

  DioClientHttp(this._dio);

  @override
  void setBaseUrl(String url) {
    _dio.options.baseUrl = url;
  }

  @override
  void setHeaders(Map<String, String> header) {
    _dio.options.headers = header;
  }

  @override
  Future<BaseResponse> get(String path) async {
    await setToken();
    late Response response;
    try {
      response = await _dio.get(path);
    } on DioError catch (e) {
      if (e.response!.statusCode == 403) {
        await updateToken();
        await setToken();
        response = await _dio.get(path);
      }
    }

    return _responseAdapter(response);
  }

  @override
  Future<BaseResponse> post(
    String path, {
    Map<String, dynamic>? data,
  }) async {
    await setToken();
    late Response response;
    try {
      response = await _dio.post(path, data: data);
    } on DioError catch (e) {
      if (e.response!.statusCode == 403) {
        await updateToken();
        await setToken();
        response = await _dio.post(path, data: data);
      }
    }

    return _responseAdapter(response);
  }

  @override
  Future<BaseResponse> loginPost(
    String path, {
    Map<String, dynamic>? data,
  }) async {
    final response = await _dio.post(path, data: data);
    return _responseAdapter(response);
  }

  @override
  Future<BaseResponse> delete(String path) async {
    await setToken();
    late Response response;
    try {
      response = await _dio.delete(path);
    } on DioError catch (e) {
      if (e.response!.statusCode == 403) {
        await updateToken();
        await setToken();
        response = await _dio.delete(path);
      }
    }

    return _responseAdapter(response);
  }

  @override
  Future<BaseResponse> put(
    String path, {
    Map<String, dynamic>? data,
  }) async {
    await setToken();
    late Response response;
    try {
      response = await _dio.put(path, data: data);
    } on DioError catch (e) {
      if (e.response!.statusCode == 403) {
        await updateToken();
        await setToken();
        response = await _dio.put(path, data: data);
      }
    }

    return _responseAdapter(response);
  }

  BaseResponse _responseAdapter(Response response) {
    return BaseResponse(
      response.data,
      BaseRequest(
        url: response.requestOptions.path,
        method: response.requestOptions.method,
        headers: response.requestOptions.headers.cast(),
        data: response.requestOptions.data,
      ),
    );
  }

  Future<void> setToken() async {
    Modular.isModuleReady<AppModule>();

    final storage = Modular.get<ILocalStorage>();
    final accessToken = storage.getData(ConstStringUrl.jWToken);
    final baseOptions = BaseOptions(
      baseUrl: ConstStringUrl.urlBase,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $accessToken'
      },
    );
    _dio.options = baseOptions;
  }

  Future<void> updateToken() async {
    Map<String, dynamic> paramsLogin = {
      "username": "dev001@mail.com.br",
      "password": "rmvrK2g7O7"
    };
    final response =
        await loginPost(ConstStringUrl.urlLogin, data: paramsLogin);
    final storage = Modular.get<ILocalStorage>();
    await storage.setData(
      params: SharedParams(
        key: ConstStringUrl.jWToken,
        value: response.data['token'],
      ),
    );
  }
}

