abstract class IAppException implements Exception {
  final String message;
  final StackTrace? stackTrace;
  const IAppException({required this.message, required this.stackTrace});
}


class SharedPreferencesException implements Exception {
  final String message;
  final StackTrace? stackTrace;

  SharedPreferencesException(
    this.message, {
    this.stackTrace,
  });
}





