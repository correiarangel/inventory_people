import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'shared/services/client_http/dio_client_http.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'shared/services/client_http/i_client_http.dart';
import 'shared/services/overlay/asuka_overlay_service.dart';
import 'shared/services/shared_preference/i_local_storege_service.dart';
import 'shared/services/shared_preference/shared_preferences_service.dart';

Bind<Dio> _dioFactory() {

  return Bind.factory<Dio>((i) => Dio(), export: true);
}

class CoreModule extends Module {
  @override
  final List<Bind> binds = [
    _dioFactory(),
    Bind.factory<IClientHttp>((i) => DioClientHttp(i()), export: true),
    AsyncBind<SharedPreferences>((i) => SharedPreferences.getInstance(),
        export: true),
    Bind<ILocalStorage>(((i) => SharedPreferencesService(i())), export: true),
   // Bind((i) => DioClientInterceptor(dio: i(),storage: i()), export: true),
    Bind.singleton((i) => AsukaOverlayService(), export: true),

  ];
}
  