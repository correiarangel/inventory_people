import 'dart:ui';

import 'package:flutter/material.dart';

class ConstColors {
  static const Color colorPalatinatePurple = Color(0xff520049);
  static const Color colorPrimaryMardiGras = Color(0xff8e037f);
  static const Color colorMagenta = Color(0xfff910e6);
  static const Color colorMagnolia = Color(0xfff6edf5);
  static const Color colorCrimson = Color(0xffd72638);
  static const Color colorFrorestGreen = Color(0xff248232);
  static const Color colorWhite = Color(0xffffffff);
  static const Color colorSpanishGrey = Color(0xffa29c9b);
  static const Color colorBlackOlive = Color(0xff353831);
}
