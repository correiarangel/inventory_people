
import 'package:flutter_modular/flutter_modular.dart';
import 'package:inventory_people/app/modules/person/person_module.dart';
import 'modules/register/register_module.dart';

import 'core/core_module.dart';
import 'modules/login/login_module.dart';


class AppModule extends Module {
  @override
  final List<Module> imports = [
    CoreModule(),
    LoginModule(),
    PersonModule(),
  ];
  
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute('/', module: LoginModule()),
    ModuleRoute('/', module: RegisterModule()),
    //ChildRoute('/register', child: ((context, args) => const RegisterPage())),
  ];
}
