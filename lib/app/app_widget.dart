import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'core/shared/services/navigation_service/navigation_service.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Modular.setNavigatorKey(NavigationService.navigatorKey);
    Modular.setObservers([asuka.asukaHeroController]);
    return MaterialApp.router(
      routeInformationParser: Modular.routeInformationParser,
      routerDelegate: Modular.routerDelegate,
      title: 'Inventory of people',
      builder: asuka.builder,
      themeMode: ThemeMode.light,
      darkTheme: ThemeData.light(),
      theme: ThemeData(primarySwatch: Colors.purple),
    );
  }
}
