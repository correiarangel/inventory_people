import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/core/shared/services/client_http/dio_client_http.dart';
import 'package:inventory_people/app/modules/login/domain/repositories/i_login_repository.dart';
import 'package:inventory_people/app/modules/login/domain/usecases/login_usecase.dart';
import 'package:inventory_people/app/modules/login/infra/datasources/i_login_datasource.dart';
import 'package:inventory_people/app/modules/login/presenter/models/current_user_model.dart';
import 'package:inventory_people/app/modules/login/presenter/models/param_login_model.dart';
import 'package:inventory_people/app/modules/person/domain/entities/person_entity.dart';
import 'package:inventory_people/app/modules/person/domain/repositories/i_person_repository.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/delete_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_all_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/register_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/update_person.dart';
import 'package:inventory_people/app/modules/person/domain/value_objects/type_profiles.dart';
import 'package:inventory_people/app/modules/person/infra/datasources/i_person_datasource.dart';

class DioMock extends Mock implements DioForNative {}

class DioClientHttpMock extends Mock implements DioClientHttp {}

class ResponseMock extends Mock implements Response {}

class DioErrorMock extends Mock implements DioError {}

class RequestOptionsMock extends Mock implements RequestOptions {}

class PersonEntityMock extends Mock implements PersonEntity {}

class PersonRepositoryMock extends Mock implements IPersonRepository {}

class PersonDatasourceMock extends Mock implements IPersonDatasource {}

class LoginRepositoryMock extends Mock implements ILoginRepository {}

class LoginDatasourceMock extends Mock implements ILoginDatasource {}

class LoginUsecaseMock extends Mock implements ILoginUsecase {}

class GetAllPersonUsecaseMock extends Mock implements IGetAllPersonUsecase {}

class GetPersonUsecaseMock extends Mock implements IGetPersonUsecase {}

class RegisterPersonUsecaseMock extends Mock implements IRegisterPersonUsecase {
}

class UpdatePersonUsecaseMock extends Mock implements IUpdatePersonUsecase {}

class DeletePersonUsecaseMock extends Mock implements IDeletePersonUsecase {}

final personEntity = PersonEntity(
  id: '627d747d183f127b99e1a673',
  cpf: '221.856.590-06',
  name: 'Marcos F.C. Rangel',
  email: 'correiarangel@bol.com.br',
  profiles: Profiles.user,
);

final Map<String, dynamic> map = {
  'cpf': '221.856.590-06',
  'name': 'Marcos F.C. Rangel',
  'email': 'correiarangel@bol.com.br',
  'password': '',
  'profiles': ['USER']
};
final Map<String, Object> mapObject = {
  "id": "627d747d183f127b99e1a673",
  "cpf": "221.856.590-06",
  "name": "Marcos F.C. Rangel",
  "email": "correiarangel@bol.com.br",
  "password": "[protected]",
  "profiles": ["USER"]
};

final Map<String, dynamic> mapForConvert = {
  'id': '627d747d183f127b99e1a673',
  'cpf': '221.856.590-06',
  'name': 'Marcos F.C. Rangel',
  'email': 'correiarangel@bol.com.br',
  'password': '',
  'profiles': ['USER']
};

const String idMock = '627d747d183f127b99e1a673';

const data = [
  {
    "id": "6214192d959e140e7834266f",
    "cpf": "292.075.830-62",
    "name": "Wedo Admin",
    "email": "wedo.admin@wedotec.com.br",
    "password": "[protected]",
    "profiles": ["ADMINISTRATOR"]
  },
  {
    "id": "62141c749838a1372034d533",
    "cpf": "767.172.670-20",
    "name": "Wedo Manager",
    "email": "wedo.manager@wedotec.com.br",
    "password": "[protected]",
    "profiles": ["MANAGER"]
  },
  {
    "id": "6214c2bde0039b37c5297aa6",
    "cpf": "628.870.290-02",
    "name": "Emilson Silva",
    "email": "emilson.silva@mail.com.br",
    "password": "[protected]",
    "profiles": ["MANAGER"]
  },
  {
    "id": "621652dc9c00666d52877913",
    "cpf": "999.999.999-99",
    "name": "dev001",
    "email": "dev001@mail.com.br",
    "password": "[protected]",
    "profiles": ["MANAGER"]
  },
  {
    "id": "62307e7fb522f55d35f432b6",
    "cpf": "123.456.789-09",
    "name": "marcio",
    "email": "oi@oi.com",
    "password": "[protected]",
    "profiles": ["USER"]
  },
  {
    "id": "62307f13b522f55d35f432b7",
    "cpf": "221.111.111-11",
    "name": "pollesi",
    "email": "teste@teste.com",
    "password": "[protected]",
    "profiles": ["USER"]
  },
  {
    "id": "62308d84b522f55d35f432b8",
    "cpf": "999.999.999-00",
    "name": "teste.doido",
    "email": "teste@doido.com",
    "password": "[protected]",
    "profiles": ["USER"]
  },
  {
    "id": "6230cb76942f6337d8a6bcbe",
    "cpf": "111.111.111-11",
    "name": "Carlos",
    "email": "Carlos@Carlos.com",
    "password": "[protected]",
    "profiles": ["USER"]
  },
  {
    "id": "627d747d183f127b99e1a673",
    "cpf": "221.856.590-06",
    "name": "Marcos F.C. Rangel",
    "email": "correiarangel@bol.com.br",
    "password": "[protected]",
    "profiles": ["USER"]
  }
];

const String username = 'dev001@mail.com.br';
const String password = 'rmvrK2g7O7';

const paramModel = ParamLoginModel(email: username, password: password);

final currentUserModelMock = CurrentUserModel(
  userEmail: 'dev001@mail.com.br',
  tokenType: 'bearer',
  token: token,
);

final Map<String, String> mapCurrentUser = {
  'userEmail': 'dev001@mail.com.br',
  'tokenType': 'bearer',
  'token': token,
};

const String token =
    '''eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJQRVJTT04iLCJzdWIiOiJhbnlTdWJqZWN0IiwidXNlcm5hbWUiOiJkZXYwMDFAbWFpbC5jb20uYnIiLCJpYXQiOjE2NTI2NjE0MzUsImV4cCI6MTY1MjY2MjAzNX0.ovSYv6BVCEGxnWappGtiNOXsKK8JAYqt_N222t4_v84''';
