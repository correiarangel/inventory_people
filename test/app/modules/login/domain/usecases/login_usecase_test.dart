import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:inventory_people/app/modules/login/domain/repositories/i_login_repository.dart';
import 'package:inventory_people/app/modules/login/domain/usecases/login_usecase.dart';
import 'package:inventory_people/app/modules/login/failures/login_errors.dart';
import 'package:inventory_people/app/modules/login/infra/adapter/current_user_to_json.dart';
import 'package:mocktail/mocktail.dart';


import '../../../../../mocks/mocks.dart';

void main() {
  late ILoginRepository repository;

  setUp(() {
    repository = LoginRepositoryMock();
  });


  group('Caminho Feliz ;]', () {
    test('Deve completar login ...', () async {
      when(() => repository.login(email: username, password: password))
          .thenAnswer(
        (_) async => right(CurrentUserToJson.fromMap(mapCurrentUser)),
      );
      final usecase = LoginUsecase(repository);

      expect(usecase(email: username, password: password), completes);
    });

    test(
        'Deve completar login retorna '
        'CurrentUserEnptity contendo token...', () async {
      when(() => repository.login(email: username, password: password))
          .thenAnswer(
        (_) async => right(CurrentUserToJson.fromMap(mapCurrentUser)),
      );
      final usecase = LoginUsecase(repository);
      final result = await usecase(email: username, password: password);
      expect(
        result.fold((l) => l, (r) {
          return r.token;
        }),
        token,
      );
    });
  });

  group('Caminho Triste :[', () {
    test('Deve retornar ERROR:  password must contain 8 characters ...',
        () async {
      when(() => repository.login(email: username, password: '123')).thenThrow(
        (_) async =>  left(const LoginError(message: 'erro', stackTrace: null)),
      );
      final usecase = LoginUsecase(repository);
      final result = await usecase(email: username, password: '123');

      expect(result.fold((l) => l, (r) => r), isA<LoginError>());
      expect(
        result.fold((l) {
          return l.message
              .contains('ERROR: password');
        }, (r) => r),
        true,
      );
    });
    test('Deve retornar  ERROR: Invalid email or password, case passwod is empty string ...',
        () async {
      when(() => repository.login(email: username, password: '')).thenThrow(
        (_) async =>  left(const LoginError(message: 'erro', stackTrace: null)),
      );
      final usecase = LoginUsecase(repository);
      final result = await usecase(email: username, password: '');

      expect(result.fold((l) => l, (r) => r), isA<LoginError>());
      expect(
        result.fold((l) {
          return l.message.contains('ERROR: Invalid email or password');
        }, (r) => r),
        true,
      );
    });

    test('Deve se email vazia retornar ERROR: Invalid email or password ...',
        () async {
      when(() => repository.login(email: '', password: password)).thenThrow(
        (_) async =>  left(const LoginError(message: 'erro', stackTrace: null)),
      );
      final usecase = LoginUsecase(repository);
      final result = await usecase(email: '', password: password);

      expect(result.fold((l) => l, (r) => r), isA<LoginError>());
      expect(
        result.fold((l) {
          return l.message.contains('ERROR: Invalid email or password');
        }, (r) => r),
        true,
      );
    });

    test('Deve se email vazia retornar erro  ERROR:email must contain @...',
        () async {
      when(() => repository.login(email: 'paulodealask.com', password: password))
          .thenThrow(
        (_) async =>  left(const LoginError(message: 'erro', stackTrace: null)),
      );
      final usecase = LoginUsecase(repository);
      final result =
          await usecase(email: 'paulodealask.com', password: password);

      expect(result.fold((l) => l, (r) => r), isA<LoginError>());
      expect(
        result.fold((l) {
          return l.message.contains('ERROR:email must contain @');
        }, (r) => r),
        true,
      );
    });
  });
}
