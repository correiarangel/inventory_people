import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/modules/login/infra/adapter/login_param_to_json.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  group('Params to Map', () {
    test('Deve converter 2 parametros em Map<String,dynamic> ...', () async {
      final person =
          LoginParamToJson.toMap(email: username, password: password);

      expect(person, isA<Map<String, dynamic>>());
    });

    test('Deve conter email no parametro username  ...', () async {
      final person =
          LoginParamToJson.toMap(email: username, password: password);

      expect(person['username'], username);
    });
  });
}
