import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/modules/login/domain/entities/currenty_user_entity.dart';
import 'package:inventory_people/app/modules/login/infra/adapter/current_user_to_json.dart';

import '../../../../../mocks/mocks.dart';

void main() {

  group('CurrentUserEnptity To Map', () {
    test('Deve converter CurrentUserEnptity em Map<String,dynamic> ...',
        () async {
      final person = CurrentUserToJson.toMap(currentUserModelMock);

      expect(person, isA<Map<String, dynamic>>());
    });

    test('Deve converter token após converção ...', () async {
      final person = CurrentUserToJson.toMap(currentUserModelMock);

      expect(person['token'], token);
    });
  });

  group('Map To CurrentUserEnptity ', () {
    test('Deve converter  Map<String,dynamic> em CurrentUserEnptity ...',
        () async {
      final person = CurrentUserToJson.fromMap(mapCurrentUser);

      expect(person, isA<CurrentUserEnptity>());
    });

    test('Deve converter email igual a dev001@mail.com.br após converção ...',
        () async {
      final person = CurrentUserToJson.fromMap(mapCurrentUser);

      expect(person.userEmail, 'dev001@mail.com.br');
    });
  });
}
