import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/modules/login/domain/repositories/i_login_repository.dart';
import 'package:inventory_people/app/modules/login/failures/login_errors.dart';
import 'package:inventory_people/app/modules/login/infra/adapter/login_param_to_json.dart';
import 'package:inventory_people/app/modules/login/infra/datasources/i_login_datasource.dart';
import 'package:inventory_people/app/modules/login/infra/repositories/login_reposytory.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  late ILoginDatasource _datasource;
  late ILoginRepository repository;

  setUp(() {
    _datasource = LoginDatasourceMock();
     repository = LoginRepository(_datasource);
  });

  group('LoginRepository Caminho Feliz ;] ', () {
    test('Deve completar login ...', () async {
      final param = LoginParamToJson.toMap(email: username, password: password);
      when(() => _datasource.login(mapData: param))
          .thenAnswer((_) async => mapCurrentUser);
   

      expect(repository.login(email: username, password: password), completes);
    });

    test('Deve completar login conter tokenType igual a bearer ...', () async {
      final param = LoginParamToJson.toMap(email: username, password: password);

      when(() => _datasource.login(mapData: param))
          .thenAnswer((_) async => mapCurrentUser);

 
      final result =
          await repository.login(email: username, password: password);

      expect(
          result.fold((l) => l, (r) {
            return r.tokenType;
          }),
          'bearer');
    });
  });

  group('LoginRepository Caminho Triste :[', () {
    test('Deve completar retornar LoginError...', () async {
      final param = LoginParamToJson.toMap(email: username, password: '');
      when(() => _datasource.login(mapData: param))
          .thenThrow((_) async => Exception());

      final result = await repository.login(email: username, password: '');
      expect(result.fold((l) => l, (r) => r), isA<LoginError>());
    });

   
  });
}
