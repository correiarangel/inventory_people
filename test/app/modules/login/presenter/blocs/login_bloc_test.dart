import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:inventory_people/app/modules/login/domain/usecases/login_usecase.dart';
import 'package:inventory_people/app/modules/login/presenter/blocs/event/login_event.dart';
import 'package:inventory_people/app/modules/login/presenter/blocs/login_bloc.dart';
import 'package:inventory_people/app/modules/login/presenter/blocs/state/login_state.dart';
import 'package:mocktail/mocktail.dart';


import '../../../../../mocks/mocks.dart';

void main() {
  late ILoginUsecase _usecase;

  setUp(() {
    debugPrint('Iniciando testes ...');
    _usecase = LoginUsecaseMock();
  });
 
  group(
    'Caminho Feliz ;]',
    () {
      blocTest<LoginBloc, AuthState>(
        'Deve retorna estados AuthLoadingState AuthLoadedState',
        build: () {
          when(() => _usecase.call(email: username, password: password))
              .thenAnswer((_) => (Future.value(right(currentUserModelMock),)));
          return LoginBloc(_usecase);
        },
        act: (bloc) => bloc.add(LoginEvent(paramModel)),
        expect: () => [
          isA<AuthLoadingState>(),
          isA<AuthSuccessState>(),
        ],
      );
    },
  );
}
