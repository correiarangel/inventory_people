import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/core/shared/services/client_http/i_client_http.dart';
import 'package:inventory_people/app/core/shared/value/const_string_url.dart';
import 'package:inventory_people/app/modules/login/external/login_datasource.dart';
import 'package:inventory_people/app/modules/login/failures/login_errors.dart';
import 'package:inventory_people/app/modules/login/infra/adapter/login_param_to_json.dart';
import 'package:inventory_people/app/modules/login/infra/datasources/i_login_datasource.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks/mocks.dart';

void main() {
  late IClientHttp clientHttp;
  late ILoginDatasource datasource;

  setUp(() {
    clientHttp = DioClientHttpMock();
    datasource = LoginDatasource(clientHttp);
  });

  group('Caminho feliz ;]', () {
    test('Deve completar login   ...', () async {
      final param = LoginParamToJson.toMap(email: username, password: password);

      when(() => clientHttp.post(ConstStringUrl.urlLogin, data: param))
          .thenAnswer(
        (_) async => BaseResponse(
          mapCurrentUser,
          BaseRequest(
            data: {},
            method: 'post',
            headers: {},
            url: '/login',
          ),
        ),
      );

      expect(datasource.login(mapData: param), completes);
    });

    test('Deve retornar email dev001@mail.com.br...', () async {
      const url = ConstStringUrl.urlLogin;
      final param = LoginParamToJson.toMap(email: username, password: password);
      when(() => clientHttp.post(url, data: param)).thenAnswer(
        (_) async => BaseResponse(
          mapCurrentUser,
          BaseRequest(
            data: {},
            method: 'post',
            headers: {},
            url: '/login',
          ),
        ),
      );

      final result = await datasource.login(mapData: param);

      expect(result['userEmail'], 'dev001@mail.com.br');
    });
  });

  group('Caminho trite :[', () {
    test('Deve retornar um LoginError ...', () async {
      var url = ConstStringUrl.urlLogin;
      final param = LoginParamToJson.toMap(
        email: username,
        password: password,
      );

      when(() => clientHttp.post(url, data: param)).thenAnswer(
        (_) async => BaseResponse(
          {},
          BaseRequest(
            data: {},
            method: 'post',
            headers: {},
            url: '/login',
          ),
          401,
        ),
      );

      final result = await datasource.login(mapData: param);

      expect(result, isA<LoginError>());
    });
  });
}
