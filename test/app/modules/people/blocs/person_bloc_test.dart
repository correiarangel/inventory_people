import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:inventory_people/app/modules/people/blocs/event/person_event.dart';
import 'package:inventory_people/app/modules/people/blocs/person_bloc.dart';
import 'package:inventory_people/app/modules/people/blocs/state/person_state.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/delete_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_all_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/register_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/update_person.dart';

import '../../../../mocks/mocks.dart';

void main() {
  late IUpdatePersonUsecase _updateusecase;
  late IDeletePersonUsecase _deleteusecase;
  late IRegisterPersonUsecase _registerusecase;
  late IGetPersonUsecase _getPersonusecase;
  late IGetAllPersonUsecase _getAllPersonusecase;
  late PersonBloc personBloc;

  setUp(() {
    debugPrint('Iniciando testes ...');
    _getAllPersonusecase = GetAllPersonUsecaseMock();
    _getPersonusecase = GetPersonUsecaseMock();
    _registerusecase = RegisterPersonUsecaseMock();
    _updateusecase = UpdatePersonUsecaseMock();
    _deleteusecase = DeletePersonUsecaseMock();
    personBloc = PersonBloc(
      _getAllPersonusecase,
      _getPersonusecase,
      _registerusecase,
      _updateusecase,
      _deleteusecase,
    );
  });

  group('Caminho Feliz ;]', () {
    blocTest<PersonBloc, PersonState>(
      'Deve retorna estados LoadingPersonState'
      'LoadedPersonState GetAllPersonEvent',
      build: () {
        when(() => _getAllPersonusecase())
            .thenAnswer((_) => (Future.value(right([personEntity]))));
        return personBloc;
      },
      act: (bloc) => bloc.add(GetAllPersonEvent()),
      expect: () => [
        isA<LoadingPersonState>(),
        isA<LoadedPersonState>(),
      ],
    );

    blocTest<PersonBloc, PersonState>(
      'Deve retorna Lista de estados vazia GetPersonEvent',
      build: () {
        when(() => _getPersonusecase(idMock))
            .thenAnswer((_) => (Future.value(right(personEntity))));
        return personBloc;
      },
      act: (bloc) => bloc.add(GetPersonEvent(idMock)),
      expect: () => [],
    );

    blocTest<PersonBloc, PersonState>(
      'Deve retorna Lista de estados vazia RemovePersonEvent',
      build: () {
        when(() => _deleteusecase(idMock))
            .thenAnswer((_) => (Future.value(right(true))));
        return personBloc;
      },
      act: (bloc) => bloc.add(RemovePersonEvent(personEntity)),
      expect: () => [],
    );

    blocTest<PersonBloc, PersonState>(
      'Deve retorna Lista de estados vazia UpdatePersonEvent',
      build: () {
        when(() => _updateusecase(person: personEntity, id: idMock))
            .thenAnswer((_) => (Future.value(right(true))));
        return personBloc;
      },
      act: (bloc) => bloc.add(UpdatePersonEvent(personEntity)),
      expect: () => [],
    );
  });
}
