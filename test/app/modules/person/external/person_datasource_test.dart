import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/core/shared/services/client_http/i_client_http.dart';
import 'package:inventory_people/app/core/shared/value/const_string_url.dart';
import 'package:inventory_people/app/modules/person/external/person_datasource.dart';
import 'package:inventory_people/app/modules/person/failures/person_errors.dart';
import 'package:inventory_people/app/modules/person/infra/adapter/person_to_json_adapter.dart';
import 'package:inventory_people/app/modules/person/infra/datasources/i_person_datasource.dart';

import '../../../../mocks/mocks.dart';

void main() {
  late IClientHttp client;
  late IPersonDatasource datasource;

  setUp(() {
    debugPrint('Iniciando testes ...');
    client = DioClientHttpMock();
    datasource = PersonDatasource(client);
  });
  group('Caminho feliz ;]', () {
    test('Deve retornar List<Map<String,dynamic>>>  ...', () async {
      var url = ConstStringUrl.urlBase;
      when(() => client.get(url)).thenAnswer(
        (_) async => BaseResponse(
          [mapForConvert],
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          200,
        ),
      );

      final personList = await datasource.getAll(url: url);

      expect(personList, isA<List<Map<String, dynamic>>>());
    });

    test('Deve retornar List List Map<String,dynamic> com 1 posição ...',
        () async {
      var url = ConstStringUrl.urlBase;
      when(() => client.get(url)).thenAnswer(
        (_) async => BaseResponse(
          [mapForConvert],
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          200,
        ),
      );

      final personList = await datasource.getAll(url: url);

      expect(personList.length, 1);
    });

    test('Deve retornar Map<String,dynamic>   ... ...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';
      when(() => client.get(url)).thenAnswer(
        (_) async => BaseResponse(
          mapForConvert,
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          200,
        ),
      );

      final future = await datasource.get(url: url);

      expect(future, isA<Map<String, dynamic>>());
    });

    test('Deve retornar name igual a Marcos F.C. Rangel...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';
      when(() => client.get(url)).thenAnswer(
        (_) async => BaseResponse(
          mapForConvert,
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          200,
        ),
      );

      final future = await datasource.get(url: url);

      expect(future['name'], 'Marcos F.C. Rangel');
    });

    test('Deve registrar um novo PersonEntity retornat true  ...', () async {
      var url = ConstStringUrl.urlBase;
      final param = PersonToJsonAdapter.toMap(personEntity);
      param.remove('id');

      when(() => client.post(url, data: param)).thenAnswer(
        (_) async => BaseResponse(
          {},
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          201,
        ),
      );

      final result = await datasource.register(url: url, paramsMap: param);

      expect(result, true);
    });
    test('Deve atulisar e retornar um  PersonEntity atulisado...', () async {
      const url = '${ConstStringUrl.urlBase}$idMock';
      final param = PersonToJsonAdapter.toMap(personEntity);
      param.remove('id');

      when(() => client.put(url, data: param)).thenAnswer(
        (_) async => BaseResponse(
          {},
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          200,
        ),
      );

      expect(datasource.update(paramsMap: param, url: url), completes);
    });

    test('Deve deletar um Person e retonar true  ...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';

      when(() => client.delete(url)).thenAnswer(
        (_) async => BaseResponse(
          {},
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          200,
        ),
      );

      final result = await datasource.delete(url: url);

      expect(result, isA<bool>());
    });
  });

  group('Caminho trite :[', () {
    test(
        'Deve retornar GetAllPersonError em '
        'caso de erro ao buscar AllPerson...', () async {
      var url = ConstStringUrl.urlBase;
      when(() => client.get(url)).thenAnswer(
        (_) async => BaseResponse(
          [mapForConvert],
          BaseRequest(
            data: {},
            method: 'get',
            headers: {},
            url: url,
          ),
          403,
        ),
      );

      final future = await datasource.getAll(url: url);

      expect(future, isA<PersonError>());
    });

    test(
        'Deve retornar PersonError em caso de'
        ' erro ao buscar Person ...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';
      when(() => client.get(url)).thenAnswer((_) async => BaseResponse(
            [mapForConvert],
            BaseRequest(
              data: {},
              method: 'get',
              headers: {},
              url: url,
            ),
            403,
          ));

      final future = await datasource.get(url: url);

      expect(future, isA<PersonError>());
    });

    test(
        'Deve retorna PersonError'
        ' em caso de erro ao registrar ...', () async {
      var url = ConstStringUrl.urlBase;
      final param = PersonToJsonAdapter.toMap(personEntity);
      param.remove('id');

      when(() => client.post(url, data: param))
          .thenAnswer((_) async => BaseResponse(
                [mapForConvert],
                BaseRequest(
                  data: {},
                  method: 'get',
                  headers: {},
                  url: url,
                ),
                403,
              ));

      final future = datasource.register(paramsMap: param, url: url);

      expect(future, isA<PersonError>());
    });

    test(
        'Deve retornar PersonError em'
        ' caso de erro ao atulisado...', () async {
      const url = '${ConstStringUrl.urlBase}$idMock';
      final param = PersonToJsonAdapter.toMap(personEntity);
      param.remove('id');

      when(() => client.put(url, data: param))
          .thenAnswer((_) async => BaseResponse(
                [mapForConvert],
                BaseRequest(
                  data: {},
                  method: 'get',
                  headers: {},
                  url: url,
                ),
                403,
              ));

      final future = datasource.update(url: url, paramsMap: param);

      expect(future, isA<PersonError>());
    });

    test(
        'Deve retornar PersonError em caso'
        ' de erro ao deletar uma Perssoa  ...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';

      when(() => client.delete(url)).thenAnswer((_) async => BaseResponse(
            [mapForConvert],
            BaseRequest(
              data: {},
              method: 'get',
              headers: {},
              url: url,
            ),
            403,
          ));
      final future = datasource.delete(url: url);

      expect(future, isA<PersonError>());
    });
  });
}
