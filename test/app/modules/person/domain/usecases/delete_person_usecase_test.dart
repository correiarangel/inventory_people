import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:inventory_people/app/modules/person/domain/repositories/i_person_repository.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/delete_person_usecase.dart';
import 'package:inventory_people/app/modules/person/failures/person_errors.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  late IPersonRepository repository;

  setUp(() {
    repository = PersonRepositoryMock();
  });

  group('Caminho Feliz ;]', () {
    test('Deve deletar um PersonEntite ...', () async {
      when(() => repository.deletePerson(idMock)).thenAnswer(
        (_) async => right(true),
      );
      final usecase = DeletePersonUsecase(repository);
      expect(usecase(idMock), completes);
    });

    test('Deve retornar PersonEntity  ...', () async {
      when(() => repository.deletePerson(idMock)).thenAnswer(
        (_) async => right(true),
      );
      final usecase = DeletePersonUsecase(repository);
      final result = await usecase.call(idMock);
      expect(result, isA<void>());
    });
  });

  group('Caminho Triste :[', () {
    test('Deve completar chamada mesmo com id sem valor setado ...', () async {
      when(() => repository.deletePerson('')).thenAnswer(
        (_) async => left(
          const PersonError(
              message: 'ERROR: O Parametro esta vazio', stackTrace: null),
        ),
      );
      final usecase = DeletePersonUsecase(repository);
      expect(usecase(''), completes);
    });

    test('Deve retornar PersonError caso id vazio ...', () async {
      when(() => repository.deletePerson('')).thenAnswer(
        (_) async => left(
          const PersonError(
              message: 'ERROR: O Parametro esta vazio', stackTrace: null),
        ),
      );
      final usecase = DeletePersonUsecase(repository);
      final result = await usecase('');
      expect(result.fold((l) => l, (r) => null), isA<PersonError>());
    });
  });
}
