import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:inventory_people/app/modules/person/failures/person_errors.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/modules/person/domain/entities/person_entity.dart';
import 'package:inventory_people/app/modules/person/domain/repositories/i_person_repository.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_all_person_usecase.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  late IPersonRepository repository;

  late PersonError exception;
  setUpAll(() {
    exception = const PersonError(message: 'Error', stackTrace: null);
  });
  setUp(() {
    repository = PersonRepositoryMock();
  });
  group('Caminho feliz ;]', () {
    test('Deve completar chamada  ...', () async {
      when(() => repository.getAllPerson())
          .thenAnswer((_) async => right([personEntity]));
      final usecase = GetAllPersonUsecase(repository);

      expect(usecase(), completes);
    });

    test('Deve retornar lista de PersonEntite  ...', () async {
      when(() => repository.getAllPerson())
          .thenAnswer((_) async => right([personEntity]));
      final usecase = GetAllPersonUsecase(repository);
      final result = await usecase();

      expect(result.fold((l) => l, (r) => r), isA<List<PersonEntity>>());
    });
  });

  group('Caminho trite :[', () {
    test('Deve Deve retornar Left em caso de excepition ...', () async {
      when(() => repository.getAllPerson())
          .thenAnswer((_) async => left(exception));

      final usecase = GetAllPersonUsecase(repository);
      final result = await usecase.call();

      expect(result.isLeft, true);
    });

        test('Deve Deve retornar GetAllPersonError  ...', () async {
      when(() => repository.getAllPerson())
          .thenAnswer((_) async => left(exception));

      final usecase = GetAllPersonUsecase(repository);
      final result = await usecase.call();

          expect(result.fold((l) => l, (r) => r), isA<PersonError>());
    });
  });
}
