import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/modules/person/domain/entities/person_entity.dart';
import 'package:inventory_people/app/modules/person/domain/repositories/i_person_repository.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/update_person.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  late IPersonRepository repository;
  final entite = PersonEntity(
      cpf: personEntity.cpf,
      id: personEntity.id,
      name: 'Zequinha Barsosa',
      email: 'meu@email',
      profiles: personEntity.profiles);

  setUp(() {
    debugPrint('Iniciando testes ...');
    repository = PersonRepositoryMock();
  });

  test('Deve completar chamada para updatePerson ...', () async {
    when(() => repository.registerPerson(person: personEntity)).thenAnswer(
      (_) async => right(true),
    );
    when(() => repository.updatePerson(person: entite, id: idMock)).thenAnswer(
      (_) async => right(true),
    );
    final usecase = UpdatePersonUsecase(repository);

    expect(usecase(person: entite, id: idMock), completes);
  });

  test('Deve retorna true caso pessao sea editada  ...', () async {
    when(() => repository.registerPerson(person: personEntity)).thenAnswer(
      (_) async => right(true),
    );
    when(() => repository.updatePerson(person: entite, id: idMock)).thenAnswer(
      (_) async => right(true),
    );
    when(() => repository.getPerson(idMock)).thenAnswer(
      (_) async => right(entite),
    );

    final usecase = UpdatePersonUsecase(repository);
    final result = await usecase(person: entite, id: idMock);
    expect(result.fold((l) => l, (r) => r), true);
  });
}
