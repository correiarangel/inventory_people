
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:inventory_people/app/modules/person/domain/entities/person_entity.dart';
import 'package:inventory_people/app/modules/person/domain/repositories/i_person_repository.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_person_usecase.dart';
import 'package:inventory_people/app/modules/person/failures/person_errors.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  late IPersonRepository repository;

  setUp(() {
    debugPrint('Iniciando testes ...');
    repository = PersonRepositoryMock();
  });

  group('Caminho Feliz ;]', () {
    test('Deve retorna um PersonEntite ...', () async {
      when(() => repository.getPerson(idMock)).thenAnswer(
        (_) async =>right(personEntity),
      );
      final usecase = GetPersonUsecase(repository);
      expect(usecase(idMock), completes);
    });

    test('Deve retornar PersonEntity  ...', () async {
      when(() => repository.getPerson(idMock)).thenAnswer(
        (_) async => right(personEntity),
      );
      final usecase = GetPersonUsecase(repository);
      final result = await usecase.call(idMock);
      expect(result.fold((l) => l, (r) => r), isA<PersonEntity>());
    });
  });

  group('Caminho Triste :[', () {
    test('Deve completar chamada mesmo com id sem valor setado ...', () async {
      when(() => repository.getPerson('')).thenAnswer(
        (_) async => left(
          const PersonError(
              message: 'ERROR: O Parametro esta vazio', stackTrace: null),
        ),
      );
      final usecase = GetPersonUsecase(repository);
      expect(usecase(''), completes);
    });

    test('Deve retornar GetPersonError  ...', () async {
      when(() => repository.getPerson('')).thenAnswer(
        (_) async =>  left(
          const PersonError(
              message: 'ERROR: O Parametro esta vazio', stackTrace: null),
        ),
      );
      final usecase = GetPersonUsecase(repository);
      final result = await usecase('');
      expect(result.fold((l) => l, (r) => r), isA<PersonError>());
    });
  });
}
