import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/core/shared/types/either.dart';
import 'package:inventory_people/app/modules/person/domain/entities/person_entity.dart';
import 'package:inventory_people/app/modules/person/domain/repositories/i_person_repository.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/register_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/value_objects/type_profiles.dart';
import 'package:inventory_people/app/modules/person/failures/person_errors.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  late IPersonRepository repository;
  final enptity = PersonEntity(
    id: '',
    name: '',
    cpf: '',
    email: '',
    profiles: Profiles.user,
  );

  setUp(() {
    debugPrint('Iniciando testes ...');
    repository = PersonRepositoryMock();
  });

  group('Caminho Feliz ;]', () {
    test('Deve completar chamada adicionar um novo  PersonEntity ...',
        () async {
      when(() => repository.registerPerson(person:  personEntity)).thenAnswer(
        (_) async => right(true),
      );
      final usecase = RegisterPersonUsecase(repository);

      expect(usecase(personEntity), completes);
    });

    test('Deve retornar void  ...', () async {
      when(() => repository.registerPerson(person: personEntity)).thenAnswer(
        (_) async => right(true),
      );
      final usecase = RegisterPersonUsecase(repository);
      final result = await usecase(personEntity);
      expect(result.fold((l) => l, (r) => null), isA<void>());
    });
  });

  group('Caminho Triste :[', () {
    test('Deve completar chamada mesmo com id sem valor setado ...', () async {
      when(() => repository.registerPerson(person:  enptity)).thenAnswer(
        (_) async => left(
          const PersonError(
              message: 'ERROR: name esta vazio', stackTrace: null),
        ),
      );
      final usecase = RegisterPersonUsecase(repository);
      expect(usecase(enptity), completes);
    });

    test('Deve retornar RegisterPersonError se com id sem valor setado ...',
        () async {
      when(() => repository.registerPerson(person:  enptity)).thenAnswer(
        (_) async => left(
          const PersonError(
              message: 'ERROR: name esta vazio', stackTrace: null),
        ),
      );
      final usecase = RegisterPersonUsecase(repository);
      final result = await usecase(enptity);
      expect(result.fold((l) => l, (r) => null), isA<PersonError>());
    });
  });
}
