import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/modules/person/domain/entities/person_entity.dart';
import 'package:inventory_people/app/modules/person/domain/value_objects/type_profiles.dart';
import 'package:inventory_people/app/modules/person/infra/adapter/json_to_person_adapter.dart';
import 'package:inventory_people/app/modules/person/infra/adapter/person_to_json_adapter.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  group('  Map to PersonEnptity', () {
    test('Deve converter um Map<String,dynamic> em PersonEnptity...', () async {
      final person = JsonToPersonAdapter.fromMap(mapForConvert);

      expect(person, isA<PersonEntity>());
    });

    test('Deve conter id, igual a 627d747d183f127b99e1a673 ...', () async {
      final person = JsonToPersonAdapter.fromMap(mapForConvert);
      expect(person.id, '627d747d183f127b99e1a673');
    });
    test('Deve conter name  Marcos F.C. Rangel...', () async {
      final person = JsonToPersonAdapter.fromMap(mapForConvert);
      expect(person.id, '627d747d183f127b99e1a673');
      expect(person.name, 'Marcos F.C. Rangel');
    });
    test('Deve conter cpf  igual a 221.856.590-06...', () async {
      final person = JsonToPersonAdapter.fromMap(mapForConvert);
      expect(person.cpf, '221.856.590-06');
    });

    test('Deve conter um Profiles tipo enum com valor administrator ...',
        () async {
      final person = JsonToPersonAdapter.fromMap(mapForConvert);
      expect(person.profiles, isA<Profiles>());
      expect(person.profiles, Profiles.administrator);
    });
  });
  group(' PersonEnptity to Map', () {
    test('Deve converter um PersonEntity em Map<String,dynamic> ...', () async {
      final map = PersonToJsonAdapter.toMap(personEntity);
      expect(map, isA<Map<String, dynamic>>());
    });

    test('Deve conter id igual  627d747d183f127b99e1a673  ...', () async {
      final map = PersonToJsonAdapter.toMap(personEntity);
      expect(map['id'], '627d747d183f127b99e1a673');
    });

    test('Deve conter cpf  igual a 221.856.590-06 ...', () async {
      final map = PersonToJsonAdapter.toMap(personEntity);
      expect(map['cpf'], '221.856.590-06');
    });

    test('Deve conter  name  Marcos F.C. Rangel...', () async {
      final map = PersonToJsonAdapter.toMap(personEntity);
      expect(map['name'], 'Marcos F.C. Rangel');
    });
  });
}
