import 'package:flutter_test/flutter_test.dart';
import 'package:inventory_people/app/modules/person/failures/person_errors.dart';
import 'package:inventory_people/app/modules/person/infra/adapter/person_to_json_adapter.dart';
import 'package:mocktail/mocktail.dart';

import 'package:inventory_people/app/core/shared/value/const_string_url.dart';
import 'package:inventory_people/app/modules/person/domain/entities/person_entity.dart';
import 'package:inventory_people/app/modules/person/domain/repositories/i_person_repository.dart';
import 'package:inventory_people/app/modules/person/infra/datasources/i_person_datasource.dart';
import 'package:inventory_people/app/modules/person/infra/repositories/person_reposytory.dart';

import '../../../../../mocks/mocks.dart';

void main() {
  late IPersonDatasource _datasource;
  late IPersonRepository repository;

  setUp(() {
    _datasource = PersonDatasourceMock();

    repository = Personrepository(_datasource);
  });
  group('Caminho Feliz ;]', () {
    test('Deve deletar um PersonEntite deletePerson...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';
      when(() => _datasource.delete(url: url)).thenAnswer(
        (_) async => true,
      );

      expect(repository.deletePerson(idMock), completes);
    });

    test('Deve retornar true  deletePerson...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';
      when(() => _datasource.delete(url: url)).thenAnswer(
        (_) async => true,
      );

      final result = await repository.deletePerson(idMock);
      expect(result.fold((l) => l, (r) => r), true);
    });

    test('Deve retornar  PersonModel getPerson...', () async {
      var url = '${ConstStringUrl.urlBase}/$idMock';
      when(() => _datasource.get(url: url)).thenAnswer(
        (_) async => mapForConvert,
      );

      final result = await repository.getPerson(idMock);
      expect(result.fold((l) => l, (r) => r), isA<PersonEntity>());
    });

    test('Deve retornar Lista PersonModel getAllPerson...', () async {
      var url = ConstStringUrl.urlBase;
      when(() => _datasource.getAll(url: url)).thenAnswer(
        (_) async => [mapForConvert],
      );

      final result = await repository.getAllPerson();
      expect(result.fold((l) => l, (r) => r), isA<List<PersonEntity>>());
    });

    test('Deve atuisar registro retornar  PersonModel updatePerson...',
        () async {
      var url = '${ConstStringUrl.urlBase}123';
      when(
        () => _datasource.update(paramsMap: map, url: url),
      ).thenAnswer((_) async => true);

      final result = await repository.updatePerson(
        person: personEntity,
        id: '123',
      );

      expect(result.isRight, true);
/*       expect(()=>
        repository.updatePerson(person: personEntity, id: '123'),
      completes
      ); */
    });

    test('Deve  registerPerson retornar true...', () async {
      var url = '${ConstStringUrl.urlBase}123';
      final paramsMap = PersonToJsonAdapter.toMap(personEntity);

      paramsMap.remove('id');

      when(() => _datasource.register(url: url, paramsMap: paramsMap))
          .thenAnswer(
        (_) async => true,
      );

      final result = await repository.registerPerson(person: personEntity);
      expect(result.fold((l) => l, (r) => r), true);
    });
  });

  group('Caminho Triste :[', () {
    /*  test('Deve completar chamada mesmo com id vazio deletePerson...', () async {
      when(() => _datasource.deletePerson('')).thenAnswer(
        (_) async => false,
      );
    });

    test('Deve retornar false caso id vazio deletePerson...', () async {
      when(() => _datasource.deletePerson('')).thenAnswer(
        (_) async => false,
      );

      final result = await repository.deletePerson('');
      expect(result.fold((l) => l, (r) => r), isA<PersonError>());
    });

    test('Deve retornar GetPersonError se parametro fazio...', () async {
      when(() => _datasource.getPerson('')).thenThrow(
        (_) async => Exception(),
      );

      final result = await repository.getPerson('');
      expect(result.fold((l) => l, (r) => r), isA<PersonError>());
    });

    test('Deve retornar GetPersonError em caso de erro getAllPerson...',
        () async {
      when(() => _datasource.getAllPerson()).thenThrow(
        (_) async => Exception(),
      );

      final result = await repository.getAllPerson();
      expect(result.fold((l) => l, (r) => r), isA<PersonError>());
    });
*/
    test('Deve retornar UpdadtePersonError em caso de erro updatePerson...',
        () async {
      when(() => _datasource.update(paramsMap: {}, url: '')).thenThrow(
        (_) async => Exception(),
      );

      final result =
          await repository.updatePerson(person: personEntity, id: '');
      expect(result.fold((l) => l, (r) => r), isA<PersonError>());
    });

    test('Deve retornar RegisterPersonError em caso de erro updatePerson...',
        () async {
      when(() => _datasource.register(paramsMap: {}, url: '')).thenThrow(
        (_) async => Exception(),
      );

      final result =
          await repository.registerPerson(person: PersonEntityMock());
      expect(result.fold((l) => l, (r) => r), isA<PersonError>());
    });
  });
}
