import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:modular_test/modular_test.dart';

import 'package:inventory_people/app/app_module.dart';
import 'package:inventory_people/app/modules/login/domain/usecases/login_usecase.dart';
import 'package:inventory_people/app/modules/login/login_module.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/delete_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_all_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/get_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/register_person_usecase.dart';
import 'package:inventory_people/app/modules/person/domain/usecases/update_person.dart';
import 'package:inventory_people/app/modules/person/person_module.dart';

import '../mocks/mocks.dart';

void main() {
  late Dio dioMock;

  setUpAll(() {
    debugPrint('Iniciando Suite testes AppModule');
  });
  setUp(() {
    debugPrint('Iniciando testes ...');
    dioMock = DioMock();
    initModule(AppModule(), replaceBinds: [
      Bind.instance<Dio>(dioMock),
    ]);
    initModule(PersonModule());
    initModule(LoginModule());
  });

  tearDownAll(() {
    debugPrint('Finalizando Suite testes AppModule');
  });

  test('Deve recuperar  IGetAllPersonUsecase  ...', () async {
    final usecase = Modular.get<IGetAllPersonUsecase>();
    expect(usecase, isA<IGetAllPersonUsecase>());
  });
  test('Deve recuperar  IGetPersonUsecase  ...', () async {
    final usecase = Modular.get<IGetPersonUsecase>();
    expect(usecase, isA<IGetPersonUsecase>());
  });
  test('Deve recuperar  IUpdatePersonUsecase  ...', () async {
    final usecase = Modular.get<IUpdatePersonUsecase>();
    expect(usecase, isA<IUpdatePersonUsecase>());
  });
  test('Deve recuperar  IRegisterPersonUsecase  ...', () async {
    final usecase = Modular.get<IRegisterPersonUsecase>();
    expect(usecase, isA<IRegisterPersonUsecase>());
  });
  test('Deve recuperar use IDeletePersonUsecase  ...', () async {
    final usecase = Modular.get<IDeletePersonUsecase>();
    expect(usecase, isA<IDeletePersonUsecase>());
  });

  test('Deve recuperar  ILoginUsecase  ...', () async {
    final usecase = Modular.get<ILoginUsecase>();
    expect(usecase, isA<ILoginUsecase>());
  });
}
