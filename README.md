### Hi there Hello welcome ;]  <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">

<a href="https://www.linkedin.com/in/marcos-fabiano-correia-rangel/">
  <img align="left" alt="Marcos Rangel' LinkedIN" width="22px" src="https://raw.githubusercontent.com/peterthehan/peterthehan/master/assets/linkedin.svg" /> Linkdin ;]  </a> 
<br/><br/>

# inventory_people

The Person Inventory App must manage people registration.
Its development will be based on good practices, Clean Architecture, SOLID.

O App Person Inventory deve gerenciar cadastro de pessoas. 
Seu desenvolvimento sera com base em boas práticas, Clean Architecture, SOLID.

### Engineer:

- Architecture
  - Clean Architecture [(Flutterando)](https://github.com/Flutterando/Clean-Dart)
  - SOLID
  	
- State Management 
 - [BLoC](https://pub.dev/packages/flutter_bloc)
 - [Injection e Routes Modular](https://pub.dev/packages/flutter_modular)

### Back-end:
   - made available


## Project's Guide Lines

### Nomenclature

- **Folders and Files:** `snake_case`
- **Classes:** `PascalCase`
- **Variables, functions e methods:** `camelCase`
- **Interfaces:** begin with a `I`, e.g. `I`Controller
- **Sufix:** what it is, e.g. home`_page`/home`_bloc`/Home`Page` 
- **Naming Reactivity:** end with what it is, e.g. home`_bloc`/home`_state`

### Parameters Definition

Following the [Clean Architecture proposed by Flutterando](https://github.com/Flutterando/Clean-Dart#clean-dart-1) we obtain 5 layers. For each one was defined how to use paramters.

- **PRESENTER:**
    - *Reactivity:* Named
    - *Controller:* Named (Facade Design Pattern)?
    - *UI:* Named  
- **DOMAIN:** Positional
- **INFRA**: Positional
- **EXTERNAL:** Named

For every layer the rule below must be applied:
- **A sequence of parameters used in more than one place must be part of a `class`**


### Git and GitLab
- Commits: 
    - [feat, fix, doc, etc.](https://www.conventionalcommits.org/pt-br/v1.0.0/)
    - [Gitmoji](https://gitmoji.dev/)
    - Examples:

## Authors and acknowledgment
Marcos F.C.Rangel

## License
For open source projects, say how it is licensed.

#### Status do Projeto:

   - Implementado o módulo Pessoa baseado nos conceitos do Clean Dart,
     que nada mais é do que uma abordagem de arquitetura limpa, de fácil 
     compreensão, além de uma cobertura de testes superior a 65%

   - Implementado do Modulo Auth e sub mudule Login,blocs e testes 

   - Iniciado desenvolvimento de telas e integração com API 

   - Restruturando arquitetura de diretorios implementado rotas 

   
   




